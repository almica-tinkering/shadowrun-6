Normal pages (480x600)
* Overview          Core Rules Cover
* Skills
* Magic
* Resonance
* Weapons
* Grid
* Vehicles
* Living
* Development

Wizard pages (350x830)
* Type of creation
* Priorities
* Metatype          (Images of metatypes)
* Magic / Resonance
* Qualities
* Attributes
* Skill Points
* Spells
* Alchem. Preparations
* Rituals
* Adept Powers
* Complex Forms
* Personal Data


ItemType/-SubType
=================
Blades - FF "Xiphos Gladius"
Clubs  - FF "Tonfa"
Peitsche - FF "Manriki Kette"

Bow
Crossbow
Throwing

Taser
Holdout		GRW "Walther Palm Pistol"
Light		GRW "Colt America L36"
Machine		GRW "Steyer TMP"
Heavy		GRW "Ares Predator"

Submachine  GRW "FN P93 Praetor"
Shotgun     GRW "Remington Roomsweeper"
Rifle, Hunting  GRW "Ruger 101"
Rifle, Assault  GRW "Yamaha Raiden"
Rifle, SNiper   GRW "Cavalier Arms Crockett EBR"
Machine Guns    GRW "Panther XXL"

Throwers   FF "Shiawase Blazer"