/**
 * 
 */
package org.prelle.shadowrun6.jfx;

/**
 * @author Stefan
 *
 */
public enum ViewMode {

	GENERATION,
	MODIFICATION,
	VIEW_ONLY
	
}
