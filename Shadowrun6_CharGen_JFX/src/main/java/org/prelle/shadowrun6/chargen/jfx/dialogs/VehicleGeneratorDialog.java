package org.prelle.shadowrun6.chargen.jfx.dialogs;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.rpgframework.jfx.ListSection;
import org.prelle.rpgframework.jfx.ThreeColumnPane;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.listcells.DesignModValueListCell;
import org.prelle.shadowrun6.chargen.jfx.listcells.DesignOptionValueListCell;
import org.prelle.shadowrun6.chargen.jfx.listcells.QualityFactorValueListCell;
import org.prelle.shadowrun6.chargen.jfx.sections.DesignModSelector;
import org.prelle.shadowrun6.chargen.jfx.sections.DesignOptionSelector;
import org.prelle.shadowrun6.chargen.jfx.sections.QualityFactorSelector;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.fluent.SelectorWithHelp;
import org.prelle.shadowrun6.jfx.items.ItemUtilJFX;
import org.prelle.shadowrun6.vehicle.ChassisType;
import org.prelle.shadowrun6.vehicle.ConsoleType;
import org.prelle.shadowrun6.vehicle.Powertrain;
import org.prelle.shadowrun6.vehicle.QualityFactor;
import org.prelle.shadowrun6.vehiclegen.DesignModValue;
import org.prelle.shadowrun6.vehiclegen.DesignOptionValue;
import org.prelle.shadowrun6.vehiclegen.QualityFactorValue;
import org.prelle.shadowrun6.vehiclegen.VehicleGenerator;

import de.rpgframework.ResourceI18N;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class VehicleGeneratorDialog extends ManagedDialog {

	public static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(VehicleGeneratorDialog.class.getName());

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private CharacterController control;
	private VehicleGenerator vehicleGen;

	private NavigButtonControl btnControl;
	
	private ChoiceBox<ChassisType> cbChassis;
	private ChoiceBox<Powertrain> cbPowertrain;
	private ChoiceBox<ConsoleType> cbConsoleType;
	
	private VBox pane;
	private HBox flowPane;
	private Node statsNode;
	private VehicleBuildNode buildNode;
	private ThreeColumnPane paneThree;

	//-------------------------------------------------------------------
	/**
	 * @param title
	 * @param content
	 * @param buttons
	 */
	public VehicleGeneratorDialog(CharacterController ctrl) {
		super(ResourceI18N.get(RES,"dialog.title"), null, CloseType.CANCEL, CloseType.OK);
		this.control = ctrl;
		btnControl = new NavigButtonControl();
		vehicleGen = new VehicleGenerator();
		
		initCompoments();
		initLayout();
		initInteractivity();
		refresh();
	}

	//--------------------------------------------------------------------
	private void initCompoments() {
		cbChassis = new ChoiceBox<>();
		cbChassis.setConverter(new StringConverter<ChassisType>() {
			public String toString(ChassisType val) { return (val!=null)?val.getName():ResourceI18N.get(RES, "choice.chassis");}
			public ChassisType fromString(String string) {return null;}
		});
		cbChassis.getItems().addAll(ShadowrunCore.getChassisTypes());
//		Collections.sort(cbChassis.getItems(), new Comparator<ChassisType>() {
//			public int compare(ChassisType c0, ChassisType c1) {
//				int cmp = Integer.compare(c0.getType().ordinal(), c1.getType().ordinal());
//				if (cmp!=0) return cmp;
//			}
//		});
		cbPowertrain = new ChoiceBox<>();
		cbPowertrain.getItems().addAll(ShadowrunCore.getPowertrains());
		cbPowertrain.setConverter(new StringConverter<Powertrain>() {
			public String toString(Powertrain val) { return (val!=null)?val.getName():ResourceI18N.get(RES, "choice.powertrain");}
			public Powertrain fromString(String string) {return null;}
		});
		cbConsoleType = new ChoiceBox<>();
		cbConsoleType.getItems().addAll(ShadowrunCore.getConsoleTypes());
		cbConsoleType.setConverter(new StringConverter<ConsoleType>() {
			public String toString(ConsoleType val) { return (val!=null)?val.getName():ResourceI18N.get(RES, "choice.console");}
			public ConsoleType fromString(String string) {return null;}
		});
		
		buildNode = new VehicleBuildNode(vehicleGen);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		HBox sentence = new HBox(5,
				new Label(ResourceI18N.get(RES, "sentence.iambuildinga")),
				cbPowertrain,
				cbChassis,
				new Label(ResourceI18N.get(RES, "sentence.with")),
				cbConsoleType,
				new Label(ResourceI18N.get(RES, "sentence.electronics"))
				);
		sentence.setAlignment(Pos.CENTER_LEFT);
		
		flowPane = new HBox(20, buildNode);
		pane = new VBox(20,sentence, flowPane);

		paneThree = new ThreeColumnPane();
		paneThree.setHeadersVisible(false);
		
		ListSection<DesignOptionValue> lvFuncMods = new ListSection<DesignOptionValue>(ResourceI18N.get(RES, "column.functional_improvements"), null, RES) {
			protected void onAdd() {
				DesignOptionSelector selector = new DesignOptionSelector(control, vehicleGen);
				SelectorWithHelp<BasePluginData> pane = new SelectorWithHelp<BasePluginData>(selector);
				ManagedDialog dialog = new ManagedDialog(Resource.get(RES, "vehiclegen.selector.designopt.title"), pane, CloseType.OK, CloseType.CANCEL);
				CloseType result = (CloseType) getScreenManager().showAlertAndCall(dialog, selector.getButtonControl());
				logger.debug("Adding design option closed with "+result);
				if (result==CloseType.OK || result==CloseType.APPLY) {
					if (selector.isDesignOption()) {
						logger.info("Adding option: "+selector.getSelectedDesignOption());
						DesignOptionValue lic = vehicleGen.selectDesignOption(selector.getSelectedDesignOption(), selector.getSelectedLevel());
						logger.debug("Created option was "+lic);
					} else {
						logger.info("Adding modification: "+selector.getSelectedModification());
						DesignOptionValue lic = vehicleGen.selectModificationAsDesignOption(selector.getSelectedModification());
						if (selector.getSelectedModification().hasRating()) {
							lic.setLevel(selector.getSelectedLevel());
							logger.info("Set modification rating to "+selector.getSelectedLevel());
						}
						logger.debug("Created option was "+lic);
					}
					list.getItems().setAll(vehicleGen.getSelectedDesignOptions());
					VehicleGeneratorDialog.this.refresh();
				}
			}
			protected void onDelete() {
				logger.info("onDelete");
				vehicleGen.deselect(this.getListView().getSelectionModel().getSelectedItem());
				list.getItems().setAll(vehicleGen.getSelectedDesignOptions());
				VehicleGeneratorDialog.this.refresh();
			}
			public void refresh() {
				list.getItems().setAll(vehicleGen.getSelectedDesignOptions());
			}
		};
		lvFuncMods.getListView().setCellFactory(lv -> new DesignOptionValueListCell());
		paneThree.setColumn1Node(lvFuncMods);
		
		ListSection<DesignModValue> lvDesignMods = new ListSection<DesignModValue>(ResourceI18N.get(RES, "column.design_modifications"), null, RES) {
			protected void onAdd() {
				DesignModSelector selector = new DesignModSelector(control, vehicleGen);
				SelectorWithHelp<BasePluginData> pane = new SelectorWithHelp<BasePluginData>(selector);
				ManagedDialog dialog = new ManagedDialog(Resource.get(RES, "vehiclegen.selector.designmod.title"), pane, CloseType.OK, CloseType.CANCEL);
				CloseType result = (CloseType) getScreenManager().showAlertAndCall(dialog, selector.getButtonControl());
				logger.debug("Adding design mod closed with "+result);
				if (result==CloseType.OK || result==CloseType.APPLY) {
						logger.info("Adding option: "+selector.getSelected());
						DesignModValue lic = vehicleGen.selectDesignMod(selector.getSelected());
						logger.debug("Created mod was "+lic);
					list.getItems().setAll(vehicleGen.getSelectedDesignMods());
					VehicleGeneratorDialog.this.refresh();
				}
			}
			protected void onDelete() {
				logger.info("onDelete");
				vehicleGen.deselect(this.getListView().getSelectionModel().getSelectedItem());
				list.getItems().setAll(vehicleGen.getSelectedDesignMods());
				VehicleGeneratorDialog.this.refresh();
			}
			public void refresh() {
				list.getItems().setAll(vehicleGen.getSelectedDesignMods());
			}
		};
		paneThree.setColumn2Node(lvDesignMods);
		lvDesignMods.getListView().setCellFactory(lv -> new DesignModValueListCell());
		
		ListSection<QualityFactorValue> lvQualFacts = new ListSection<QualityFactorValue>(ResourceI18N.get(RES, "column.quality_factors"), null, RES) {
			protected void onAdd() {
				QualityFactorSelector selector = new QualityFactorSelector(control, vehicleGen);
				SelectorWithHelp<BasePluginData> pane = new SelectorWithHelp<BasePluginData>(selector);
				ManagedDialog dialog = new ManagedDialog(Resource.get(RES, "vehiclegen.selector.qualfact.title"), pane, CloseType.OK, CloseType.CANCEL);
				CloseType result = (CloseType) getScreenManager().showAlertAndCall(dialog, selector.getButtonControl());
				logger.debug("Adding quality factor closed with "+result);
				if (result==CloseType.OK || result==CloseType.APPLY) {
						logger.info("Adding factor: "+selector.getSelected());
						QualityFactorValue lic = vehicleGen.selectQualityFactor(selector.getSelected());
						logger.debug("Created qual.fact. was "+lic);
					list.getItems().setAll(vehicleGen.getSelectedQualityFactors());
					VehicleGeneratorDialog.this.refresh();
				}
			}
			protected void onDelete() {
				logger.info("onDelete");
				vehicleGen.deselect(this.getListView().getSelectionModel().getSelectedItem());
				list.getItems().setAll(vehicleGen.getSelectedQualityFactors());
				VehicleGeneratorDialog.this.refresh();
			}
			public void refresh() {
			}
		};
		lvQualFacts.getListView().setCellFactory(lv -> new QualityFactorValueListCell());

		paneThree.setColumn3Node(lvQualFacts);
		
		lvFuncMods.setStyle("-fx-pref-width: 20em");
		lvDesignMods.setStyle("-fx-max-width: 20em");
		lvQualFacts.setStyle("-fx-max-width: 20em");
		
		VBox content = new VBox(20,pane, paneThree);
		this.setMaxHeight(Double.MAX_VALUE);
		setContent(content);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		cbChassis.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {vehicleGen.selectChassis(n); refresh();});
		cbPowertrain.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {vehicleGen.selectPowertrain(n); refresh();});
		cbConsoleType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {vehicleGen.selectConsole(n); refresh();});
	}

	//--------------------------------------------------------------------
	private void refresh() {
		logger.debug("refresh");
		logger.info("refresh2");
		logger.warn("refresh3");
		if (statsNode!=null) flowPane.getChildren().remove(statsNode);
		
		statsNode = ItemUtilJFX.getVehicleNode(vehicleGen.getVehicle());
		if (statsNode!=null) {
			flowPane.getChildren().add(0,statsNode);
		}
		
		buildNode.updateData();
	}

	//--------------------------------------------------------------------
	public ItemTemplate getCreated() {
		return vehicleGen.getVehicle();
	}

}

class VehicleBuildNode extends GridPane {

	public static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(VehicleGeneratorDialog.class.getName());
	
	private VehicleGenerator vehicleGen;
	
	private Label lbPoints;
	private Label lbNuyen;
	private Label lbCargo;
	private Label lbSize;
	
	//---------------------------------------------------------
	public VehicleBuildNode(VehicleGenerator vehicleGen) {
		this.vehicleGen = vehicleGen;
		initComponents();
		initLayout();
		updateData();
	}
	
	//---------------------------------------------------------
	private void initComponents() {
		lbPoints= new Label();
		lbPoints.setMaxWidth(Double.MAX_VALUE);
		lbPoints.setAlignment(Pos.CENTER);
		lbNuyen = new Label();
		lbNuyen.setMaxWidth(Double.MAX_VALUE);
		lbNuyen.setAlignment(Pos.CENTER);
		lbCargo = new Label();
		lbCargo.setMaxWidth(Double.MAX_VALUE);
		lbCargo.setAlignment(Pos.CENTER);
		lbSize = new Label();
		lbSize.setMaxWidth(Double.MAX_VALUE);
		lbSize.setAlignment(Pos.CENTER);
	}
	
	//---------------------------------------------------------
	private void initLayout() {
		setHgap(0);
		Label hdNuyen = new Label(ResourceI18N.get(RES, "label.nuyen"));
		Label hdPoints= new Label(ResourceI18N.get(RES, "label.designpoints"));
		Label hdCargo = new Label(ResourceI18N.get(RES, "label.cargo"));
		Label hdSize  = new Label(ResourceI18N.get(RES, "label.size"));
		hdNuyen.getStyleClass().add("table-head");
		hdNuyen.setMaxWidth(Double.MAX_VALUE);
		hdNuyen.setAlignment(Pos.CENTER);
		hdPoints.getStyleClass().add("table-head");
		hdPoints.setMaxWidth(Double.MAX_VALUE);
		hdPoints.setAlignment(Pos.CENTER);
		hdCargo.getStyleClass().add("table-head");
		hdCargo.setMaxWidth(Double.MAX_VALUE);
		hdCargo.setAlignment(Pos.CENTER);
		hdSize.getStyleClass().add("table-head");
		hdSize.setMaxWidth(Double.MAX_VALUE);
		hdSize.setAlignment(Pos.CENTER);

		add(hdPoints, 0,0);
		add(lbPoints, 0,1);
		add(hdNuyen , 1,0);
		add(lbNuyen , 1,1);
		add(hdCargo , 2,0);
		add(lbCargo , 2,1);
		add(hdSize  , 3,0);
		add(lbSize  , 3,1);
		getColumnConstraints().add(new ColumnConstraints(70));
	}
	
	//---------------------------------------------------------
	public void updateData() {
		lbPoints.setText(String.valueOf(vehicleGen.getBuildPoints()) );
		lbNuyen.setText(String.valueOf(vehicleGen.getBuildPoints()*1000));
		lbCargo.setText(String.format("%.1f",vehicleGen.getCargoFactor()));
		lbSize .setText(String.valueOf(vehicleGen.getSizeClass()));
	}
	
}