package org.prelle.shadowrun6.chargen.jfx.listcells;

import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.charctrl.SkillController;
import org.prelle.shadowrun6.chargen.jfx.AttributePanePrimaryGen;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.StackPane;

public class SkillListCell extends ListCell<Skill> {

	private final static String NORMAL_STYLE = "skill-cell";

	private SkillController control;
	private Skill data;

	private Label lineName;
	private StackPane stack;
	private ImageView imgRecommended;
	private Label lblType;

	//-------------------------------------------------------------------
	public SkillListCell(SkillController ctrl) {
		this.control = ctrl;
		lblType = new Label();
		lblType.getStyleClass().add("type-label");
		StackPane.setAlignment(lblType, Pos.CENTER_RIGHT);

		// Content
		lineName = new Label();
		lineName.setStyle("-fx-font-weight: bold;");
		StackPane.setAlignment(lineName, Pos.CENTER_LEFT);

		// Recommended icon
		imgRecommended = new ImageView();
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);

		stack = new StackPane();
		//		stack.setAlignment(Pos.TOP_RIGHT);
		stack.getChildren().addAll(lblType, lineName, imgRecommended);
		StackPane.setAlignment(imgRecommended, Pos.TOP_RIGHT);
		this.setOnDragDetected(event -> dragStarted(event));
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Skill item, boolean empty) {
		super.updateItem(item, empty);
		data = item;

		if (empty) {
			setText(null);
			setGraphic(null);			
		} else {
			lblType.setText(item.getAttribute1().getShortName());
			lineName.setText(((Skill)item).getName());
			imgRecommended.setVisible(control.isRecommended((Skill) item));
			setGraphic(stack);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (data==null)
			return;
		content.putString("skill:"+data.getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();	
	}

}