/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.SignatureManeuver;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.dialogs.EditSignatureManeuverDialog;
import org.prelle.shadowrun6.chargen.jfx.listcells.SignatureManeuverListCell;

import javafx.scene.control.Label;

/**
 * @author prelle
 *
 */
public class SignatureManeuverSection extends GenericListSection<SignatureManeuver> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SignatureManeuverSection.class.getName());

	//---------------------------------------------------------
	public SignatureManeuverSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(title, ctrl, provider);
		list.setCellFactory( lv -> {
			SignatureManeuverListCell cell = new SignatureManeuverListCell(ctrl, provider);
			cell.setOnMouseClicked(ev -> {
				if (ev.getClickCount()==2)
					onEdit(cell.getItem(), cell);
			});
			return cell;
		});
		initPlaceholder();
		
		refresh();
		list.setStyle("-fx-pref-height: 18em; -fx-min-width: 22em; -fx-pref-width: 26em");
		
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> getDeleteButton().setDisable(n==null));
	}

	//--------------------------------------------------------------------
	private void initPlaceholder() {
		Label phSelected = new Label(RES.getString("listview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		list.setPlaceholder(phSelected);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.chargen.jfx.sections.GenericListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		logger.warn("onAdd");
//		if (!control.getConnectionController().canCreateConnection()) {
//			logger.warn("UI connection creation initated, but generator does not allow it");
//			return;
//		}
		
		SignatureManeuver toAdd = new SignatureManeuver();
		EditSignatureManeuverDialog dialog = new EditSignatureManeuverDialog(toAdd, false);
		CloseType result = (CloseType) provider.getScreenManager().showAlertAndCall(dialog, dialog.getButtonControl());
		logger.debug("Adding signature maneuver dialog closed with "+result);
		if (result==CloseType.OK || result==CloseType.APPLY) {
			logger.info("Adding signature maneuver: "+toAdd);
			control.getCharacter().addSignatureManeuver(toAdd);
			getListView().getItems().add(toAdd);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.chargen.jfx.sections.GenericListSection#onDelete()
	 */
	@Override
	protected void onDelete() {
		logger.trace("onDelete");
		SignatureManeuver toDelete = list.getSelectionModel().getSelectedItem();
		if (toDelete!=null) {
			logger.info("Try remove signature maneuver: "+toDelete);
			if (toDelete.getQuality()!=null) {
				toDelete.getQuality().setChoice(null);
				toDelete.getQuality().setChoiceReference(null);
			}
			control.getCharacter().removeSignatureManeuver(toDelete);
			list.getItems().remove(toDelete);
			list.getSelectionModel().clearSelection();
		}
	}

	//-------------------------------------------------------------------
	protected void onEdit(SignatureManeuver data, SignatureManeuverListCell cell) {
		logger.debug("onEdit");

//		EditConnectionDialog dialog = new EditConnectionDialog(data, true);
//		provider.getScreenManager().showAndWait(dialog);
//		logger.info("Edited connection "+data);
//		cell.updateItem(data, false);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		setData(control.getCharacter().getSignatureManeuvers());
//		getAddButton().setDisable(!control.getConnectionController().canCreateConnection());
	}

}
