/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ViewMode;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class SR6FirstLine extends HBox {

	protected final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	protected CharacterController parent;
	protected EquipmentController control;
	protected ViewMode mode;
	protected ScreenManagerProvider provider;

	private Label lbExpTotal;
	private Label lbExpInvested;
	protected Label lbNuyen;
	protected Button btnNuyen;
	private Label lbEssence;
	private Label lbEssenceHole;

	//-------------------------------------------------------------------
	public SR6FirstLine(ViewMode mode, CharacterController control, ScreenManagerProvider provider) {
		super(5);
		this.parent  = control;
		this.control = control.getEquipmentController();
		this.mode    = mode;
		this.provider= provider;
		initComponents();
		initLayout();
		initNonExtendedInteractivity();
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		lbExpTotal    = new Label("?");
		lbExpInvested = new Label("?");
		lbNuyen       = new Label("?");
		lbEssenceHole = new Label("?");
		lbEssence     = new Label("?");
		lbExpTotal.getStyleClass().add("base");
		lbExpInvested.getStyleClass().add("base");
		lbNuyen.getStyleClass().add("base");
		lbEssenceHole.getStyleClass().add("base");
		lbEssence.getStyleClass().add("base");

		btnNuyen      = new Button("\uE104");
		btnNuyen.getStyleClass().addAll("mini-button");
		btnNuyen.setStyle("-fx-text-fill: textcolor-highlight-primary");
	}

	//-------------------------------------------------------------------
	protected void initLayout() {
		Label hdExpTotal    = new Label(SR6Constants.RES.getString("label.ep.total")+": ");
		Label hdExpInvested = new Label(SR6Constants.RES.getString("label.ep.used")+": ");
		Label hdNuyen       = new Label(SR6Constants.RES.getString("label.nuyen")+": ");
		Label hdEssenceHole = new Label(SR6Constants.RES.getString("label.essencehole")+": ");
		Label hdEssence     = new Label(SR6Constants.RES.getString("label.essence")+": ");

		getChildren().addAll(hdExpTotal, lbExpTotal, hdExpInvested, lbExpInvested, hdNuyen, lbNuyen, btnNuyen);
		getChildren().addAll(hdEssence, lbEssence, hdEssenceHole, lbEssenceHole);
		HBox.setMargin(hdNuyen, new Insets(0,0,0,20));
		HBox.setMargin(hdEssence, new Insets(0,0,0,20));
//		getStyleClass().add("character-document-view-firstline");
	}

	//-------------------------------------------------------------------
	protected void initNonExtendedInteractivity() {
		btnNuyen.setOnAction(ev -> openEditNuyenDialog());
	}

	//-------------------------------------------------------------------
	private void openEditNuyenDialog() {
		logger.debug("openEditNuyenDialog");
		ShadowrunCharacter model = parent.getCharacter();
		
		Label lbHead = new Label(Resource.get(SR6Constants.RES,"dialog.changeNuyen.heading"));
		lbHead.getStyleClass().add("base");
		TextField tfValue = new TextField();
		tfValue.setText(String.valueOf(model.getNuyen()));
		VBox layout = new VBox(5, lbHead, tfValue);

		NavigButtonControl btnCtrl = new NavigButtonControl();
		tfValue.textProperty().addListener( (ov,o,n) -> {
			try {
				int num = Integer.parseInt(n);
				btnCtrl.setDisabled(CloseType.OK,  (num<0) );
			} catch (Exception e) {
				btnCtrl.setDisabled(CloseType.OK, true);
			}
		});
		
		provider.getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(SR6Constants.RES,"dialog.changeNuyen.title"), layout, btnCtrl);
		
		int num = Integer.parseInt(tfValue.getText());
		model.setNuyen(num);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, model));	
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		lbExpTotal.setText((model.getKarmaInvested()+model.getKarmaFree())+"");
		lbExpInvested.setText(model.getKarmaInvested()+"");
		lbNuyen.setText(model.getNuyen()+"");

		lbEssence.setText(String.format("%.2f",model.getEssence()));
		lbEssenceHole.setText(String.format("%.2f",((float)model.getEssenceHole())/1000));
	}

}
