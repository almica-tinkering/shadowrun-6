/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.listcells;

import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.items.ItemEnhancement;
import org.prelle.shadowrun6.items.ItemTemplate;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;

/**
 * @author prelle
 *
 */
public class ItemTemplateOrEnhancementListCell extends ListCell<BasePluginData> {

	private final static String NORMAL_STYLE = "equipment-cell";
	private final static String NOT_MET_STYLE = "requirement-not-met-text";

	private EquipmentController control;

	private Label lblName;
	private Label lblAvail;
	private Label lblPrice;
	private HBox  lower;
	private StackPane stack;

	//-------------------------------------------------------------------
	/**
	 */
	public ItemTemplateOrEnhancementListCell(EquipmentController ctrl) {
		this.control = ctrl;
		this.setStyle("-fx-min-width: 23em;");
		lblName = new Label();
		lblName.setStyle("-fx-font-weight: bold;");
		lblName.setMaxWidth(Double.MAX_VALUE);

		Region buffer = new Region();
		buffer.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(buffer, Priority.ALWAYS);

		lblPrice = new Label();
		lblPrice.getStyleClass().add("itemprice-label");
		lblPrice.setStyle("-fx-pref-width: 4em");

		lblAvail = new Label();
		lblAvail.setStyle("-fx-pref-width: 3em");
		lblAvail.getStyleClass().add("itemprice-label");
		lower   = new HBox(10);
		lower.setMaxWidth(Double.MAX_VALUE);
		lower.getChildren().addAll(buffer, lblAvail, lblPrice);

		stack = new StackPane();
		stack.setStyle("-fx-min-width: 20em;");
		stack.getChildren().addAll(lower, lblName);
		stack.setMaxWidth(Double.MAX_VALUE);
		StackPane.setAlignment(lower, Pos.CENTER_RIGHT);
		StackPane.setAlignment(lblName, Pos.CENTER_LEFT);
		this.setOnDragDetected(event -> dragStarted(event));
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(BasePluginData item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lblName.setText(item.getName());
			if (item instanceof ItemTemplate) {
				ItemTemplate tmp = (ItemTemplate)item;
				lblPrice.setText("\u00A5 "+tmp.getPrice());
				lblAvail.setText(tmp.getAvailability().toString());
				if (control.canBeSelected(tmp, null)) {
					lblPrice.getStyleClass().remove(NOT_MET_STYLE);
					lblAvail.getStyleClass().remove(NOT_MET_STYLE);
				} else {
					lblPrice.getStyleClass().add(NOT_MET_STYLE);
					lblAvail.getStyleClass().add(NOT_MET_STYLE);
				}
			} else if (item instanceof ItemEnhancement) {
				lblPrice.setText("");
				lblAvail.setText("");
			}
			setGraphic(stack);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (getItem()==null)
			return;
		if (getItem() instanceof ItemTemplate)
			content.putString("gear:"+getItem().getId());
		else
			content.putString("gearmod:"+getItem().getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}


}
