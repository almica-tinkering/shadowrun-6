package org.prelle.shadowrun6.chargen.jfx.listcells;

import org.prelle.javafx.SymbolIcon;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemEnhancementValue;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class ItemEnhancementValueObjectListCell extends ListCell<Object> {

	private EquipmentController control;
	
	private HBox layout;
	private Label lblSize;
	private Label lblName;
	private Button btnDel;

	//--------------------------------------------------------------------
	public ItemEnhancementValueObjectListCell(CarriedItem container, EquipmentController control) {
		this.control = control;
		lblSize = new Label();
		lblSize.getStyleClass().add("title");
		lblName  = new Label();
		lblName.getStyleClass().add("subtitle");
		btnDel   = new Button(null,new SymbolIcon("delete"));
		btnDel.getStyleClass().addAll("mini-button","title");
		btnDel.setOnAction(ev -> {
			control.remove(container, (ItemEnhancementValue) getItem());
		});

		layout = new HBox(5);
		layout.getChildren().addAll(lblName, lblSize, btnDel);
		HBox.setHgrow(lblName, Priority.ALWAYS);
		lblName.setMaxWidth(Double.MAX_VALUE);
		
//		getStyleClass().add("metatype-cell");
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Object obj, boolean empty) {
		super.updateItem(obj, empty);

		if (empty) {
			setGraphic(null);
		} else {
			ItemEnhancementValue item = (ItemEnhancementValue) obj;
			setGraphic(layout);
			if (item.getModifyable()==null) {
				lblName.setText("??Error??");
			} else {
				lblName.setText(item.getModifyable().getName());
				if (item.isAutoAdded())
					lblSize.setText(null);
				else
					lblSize.setText(""+item.getModifyable().getSize());
			}
		}
	}
}