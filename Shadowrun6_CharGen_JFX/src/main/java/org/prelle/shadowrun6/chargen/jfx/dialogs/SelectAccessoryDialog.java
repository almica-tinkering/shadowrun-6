package org.prelle.shadowrun6.chargen.jfx.dialogs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.rpgframework.jfx.DescriptionPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.UseAs;
import org.prelle.shadowrun6.jfx.SR6Constants;

import de.rpgframework.ResourceI18N;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class SelectAccessoryDialog extends ManagedDialog {
	
	private OptionalDescriptionPane paneWithDesc;
	private ChoiceBox<ItemSubType> cbSubType;
	private ListView<ItemTemplate> available;
	private DescriptionPane desc;
	private NavigButtonControl btnControl;
	private List<ItemTemplate> data;
	private ItemHook hook;

	//-------------------------------------------------------------------
	public SelectAccessoryDialog(String title, List<ItemTemplate> data, ItemHook hook, Callback<ListView<ItemTemplate>, ListCell<ItemTemplate>> cellFactory, CloseType... buttons) {
		super(title, null, buttons);
		this.data = data;
		this.hook = hook;

		initComponents();
		available.setCellFactory(cellFactory);
		initInteractivity();
		initLayout();
		
		available.getItems().addAll(data);
		
		List<ItemSubType> list = new ArrayList<>();
		for (ItemTemplate tmp : data) {
			UseAs usage = tmp.getBestAccessoryUsage(List.of(hook));
			if (!list.contains(usage.getSubtype())) {
				list.add(usage.getSubtype());
			}
		}
		cbSubType.getItems().addAll(list);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		btnControl = new NavigButtonControl();
		available = new ListView<ItemTemplate>();
		available.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		Label placeholder = new Label(ResourceI18N.get(SR6Constants.RES, "selectaccessory.placeholder"));
		placeholder.setWrapText(true);
		available.setPlaceholder(placeholder);
		desc = new DescriptionPane();
		
		cbSubType = new ChoiceBox<>();
		cbSubType.setConverter(new StringConverter<ItemSubType>() {
			public String toString(ItemSubType value) { return (value!=null)?value.getName():""; }
			public ItemSubType fromString(String string) { return null; }
		});
		
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		available.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			desc.setText(n);
		});
		
		cbSubType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			LogManager.getLogger("shadowrun6").warn("Change to  "+n);
			List<ItemTemplate> list = new ArrayList<>(data);
			if (n!=null) {
				list = list.stream().filter(item -> item.getBestAccessoryUsage(List.of(hook)).getSubtype()==n).collect(Collectors.toList());
			}
			available.getItems().setAll(list);
		});
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		VBox box = new VBox(10, cbSubType, available);
		paneWithDesc = new OptionalDescriptionPane(box, desc);
		setContent(paneWithDesc);
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//-------------------------------------------------------------------
	public Collection<ItemTemplate> getSelection() {
		return available.getSelectionModel().getSelectedItems();
	}
}
