/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.listcells;

import org.prelle.shadowrun6.vehiclegen.QualityFactorValue;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class QualityFactorValueListCell extends ListCell<QualityFactorValue> {

	private final static String NORMAL_STYLE = "equipment-cell";

	private Label lblName;
	private Label lblCost;
	private HBox layout;

	//-------------------------------------------------------------------
	/**
	 * @param asAccessory The item shall be treated as an accessory
	 */
	public QualityFactorValueListCell() {
		lblName = new Label();
		lblName.setStyle("-fx-font-weight: bold;");
		lblName.setMaxWidth(Double.MAX_VALUE);
		
		lblCost = new Label();
		
		layout = new HBox();
		layout.getChildren().addAll(lblName, lblCost);
		layout.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(lblName, Priority.ALWAYS);
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(QualityFactorValue item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lblName.setText(item.getQualityFactor().getName());
			if (Math.round(item.getMultiplier())!=item.getMultiplier()) {
				lblCost.setText(String.format("x%.1f", item.getMultiplier()));				
			} else
				lblCost.setText(String.format("x%1d",Math.round(item.getMultiplier())));
			setGraphic(layout);
		}
	}

}
