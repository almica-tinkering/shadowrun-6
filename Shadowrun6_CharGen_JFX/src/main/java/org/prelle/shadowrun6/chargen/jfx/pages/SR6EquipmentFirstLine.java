/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.pages;

import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ViewMode;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class SR6EquipmentFirstLine extends SR6FirstLine {

	private Button btnDec;
	private Button btnInc;
	private Label lbConvKarma;
	private Label lbDebt;
	private Button btnDebt;

	//-------------------------------------------------------------------
	public SR6EquipmentFirstLine(ViewMode mode, CharacterController control, ScreenManagerProvider provider) {
		super(mode, control, provider);
		initExtendedComponents();
		initExtendedLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	protected void initExtendedComponents() {
		btnDec        = new Button("\uE0C9");
		lbConvKarma   = new Label("?");
		btnInc        = new Button("\uE0C8");
		
		lbDebt        = new Label("?");
		btnDebt       = new Button("\uE0C9");
		lbConvKarma.getStyleClass().add("base");
		btnInc.getStyleClass().addAll("mini-button");
		btnInc.setStyle("-fx-text-fill: textcolor-highlight-primary");
		btnDec.getStyleClass().addAll("mini-button");
		btnDec.setStyle("-fx-text-fill: textcolor-highlight-primary");
		lbDebt.getStyleClass().add("base");
		btnDebt.getStyleClass().add("mini-button");
		btnDebt.setStyle("-fx-text-fill: textcolor-highlight-primary");
	}

	//-------------------------------------------------------------------
	protected void initExtendedLayout() {
		Label hdConvKarma   = new Label(Resource.get(SR6Constants.RES,"label.converted_karma")+": ");
		Label hdDebt        = new Label(Resource.get(SR6Constants.RES,"label.debt")+": ");

		if (mode==ViewMode.GENERATION) {
			getChildren().addAll(hdConvKarma, btnDec, lbConvKarma, btnInc);
			btnNuyen.setManaged(false);
			btnNuyen.setVisible(false);
		}
		
		getChildren().addAll(hdDebt, lbDebt);
		
		if (mode==ViewMode.MODIFICATION)
			getChildren().addAll(btnDebt);
		
		HBox.setMargin(hdConvKarma, new Insets(0,0,0,20));
		HBox.setMargin(hdDebt, new Insets(0,0,0,20));
		
		setAlignment(Pos.CENTER_LEFT);
	}

	//-------------------------------------------------------------------
	protected void initInteractivity() {
		btnDec.setOnAction(ev -> control.decreaseBoughtNuyen());
		btnInc.setOnAction(ev -> control.increaseBoughtNuyen());
		btnDebt.setOnAction(ev -> openPayDebtDialog());
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		super.setData(model);
		
		btnDec.setDisable(!control.canDecreaseBoughtNuyen());
		btnInc.setDisable(!control.canIncreaseBoughtNuyen());
		lbConvKarma.setText(String.valueOf(control.getBoughtNuyen()));
		lbDebt.setText(String.valueOf(model.getDebt()));
	}

	//-------------------------------------------------------------------
	private void openPayDebtDialog() {
		logger.debug("openPayDebtDialog");
		ShadowrunCharacter model = parent.getCharacter();
		
		Label hdYourNuyen = new Label(Resource.get(SR6Constants.RES, "label.your_nuyen"));
		Label lbYourNuyen = new Label(model.getNuyen()+"");
		Label hdYourDebt  = new Label(Resource.get(SR6Constants.RES, "label.your_debt"));
		Label lbYourDebt  = new Label(model.getDebt()+"");
		Label hdYourRPay  = new Label(Resource.get(SR6Constants.RES, "label.repayment"));
		TextField tfRPay  = new TextField("0");
		tfRPay.setPrefColumnCount(5);
		hdYourNuyen.getStyleClass().add("base");
		hdYourDebt .getStyleClass().add("base");
		hdYourRPay .getStyleClass().add("base");
		
		GridPane grid = new GridPane();
		grid.setStyle("-fx-vgap: 1em; -fx-hgap: 0.5em"); 
		grid.add(hdYourNuyen, 0, 0);
		grid.add(lbYourNuyen, 1, 0);
		grid.add(hdYourDebt , 0, 1);
		grid.add(lbYourDebt , 1, 1);
		grid.add(hdYourRPay , 0, 2);
		grid.add(    tfRPay , 1, 2);
		
		NavigButtonControl btnCtrl = new NavigButtonControl();
		tfRPay.textProperty().addListener( (ov,o,n) -> {
			try {
				int num = Integer.parseInt(n);
				btnCtrl.setDisabled(CloseType.OK,  (num>model.getNuyen() || num>model.getDebt()) );
			} catch (Exception e) {
				btnCtrl.setDisabled(CloseType.OK, true);
			}
		});
		
		provider.getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, "", grid, btnCtrl);
		
		int num = Integer.parseInt(tfRPay.getText());
		if (num>0) {
			logger.info("Reduce debt by "+num);
			model.setNuyen(model.getNuyen() - num);
			model.setDebt(model.getDebt() - num);
		}
		lbNuyen.setText(model.getNuyen()+"");
		lbDebt.setText(String.valueOf(model.getDebt()));
	}

}
