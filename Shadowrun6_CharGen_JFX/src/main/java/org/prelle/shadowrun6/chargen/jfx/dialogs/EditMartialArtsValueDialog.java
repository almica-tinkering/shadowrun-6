package org.prelle.shadowrun6.chargen.jfx.dialogs;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DescriptionPane;
import org.prelle.rpgframework.jfx.ListSection;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.LifestyleOption;
import org.prelle.shadowrun6.LifestyleOptionValue;
import org.prelle.shadowrun6.MartialArtsValue;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Technique;
import org.prelle.shadowrun6.TechniqueValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.listcells.TechniqueListCell;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.jfx.SR6Constants;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.HardcopyPluginData;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class EditMartialArtsValueDialog extends ManagedDialog implements GenerationEventListener {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(EditMartialArtsValueDialog.class.getName());

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private CharacterController control;
	private ScreenManagerProvider provider;
	private MartialArtsValue data;

	private NavigButtonControl btnControl;

	private Label lbDesc;
	private ListView<Technique> lvAvailable;
	private ListSection<TechniqueValue> lvSelected;
	private OptionalDescriptionPane content;
	private DescriptionPane description;

	//--------------------------------------------------------------------
	public EditMartialArtsValueDialog(CharacterController ctrl, MartialArtsValue toEdit, ScreenManagerProvider provider) {
		super(ResourceI18N.format(UI,"dialog.title", toEdit.getMartialArt().getName()), null, CloseType.APPLY);
		this.provider = provider;
		this.control  = ctrl;
		this.data     = toEdit;
		btnControl = new NavigButtonControl();

		initCompoments();
		initLayout();
		initInteractivity();
		refresh();

		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initCompoments() {
		lbDesc = new Label(ResourceI18N.get(UI,"dialog.desc"));
		lbDesc.setWrapText(true);
		lvAvailable = new ListView<Technique>();
		lvAvailable .setStyle("-fx-min-width: 25em;");
		lvAvailable.setCellFactory(lv -> new TechniqueListCell(control.getMartialArtsController(), data));
		lvSelected  = new ListSection<TechniqueValue>(ResourceI18N.get(UI, "techniques.selected"), provider, UI) {
			protected void onAdd() {  logger.warn("onAdd"); }
			protected void onDelete() {
				logger.warn("onDelete");
				control.getMartialArtsController().deselect(lvSelected.getListView().getSelectionModel().getSelectedItem());
			}
			public void refresh() {}
		};
		lvSelected.setAddButton(null);
		lvSelected .setStyle("-fx-min-width: 25em;");
		description = new DescriptionPane();
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		HBox layout = new HBox(20,lvAvailable, lvSelected);
		VBox vLayout = new VBox(20, lbDesc, layout);
		VBox.setVgrow(layout, Priority.ALWAYS);

		// Layout
		content = new OptionalDescriptionPane(vLayout, description);

		this.setMaxHeight(Double.MAX_VALUE);
		setContent(content);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null)
				description.setText(null);
			else if (n instanceof HardcopyPluginData) {
				description.setText((HardcopyPluginData)n);
			} 
		});
		lvSelected.getListView().getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null)
				description.setText(null);
			else {
				description.setText((HardcopyPluginData)n.getTechnique());
			} 
		});

		//		lvAvailable.setOnDragDropped(event -> dragDroppedSelected(event));
		//		lvAvailable.setOnDragOver   (event -> dragOverSelected(event));
		lvSelected.setOnDragDropped (event -> dragDroppedAvailable(event));
		lvSelected.setOnDragOver    (event -> dragOverAvailable(event));
	}

	//--------------------------------------------------------------------
	private void refresh() {
		lvAvailable.getItems().clear();
		lvSelected.getListView().getItems().clear();

		lvAvailable.getItems().addAll(control.getMartialArtsController().getAvailableTechniques(data));
		lvSelected.getListView().getItems().addAll(control.getCharacter().getTechniques(data.getMartialArt()));
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//-------------------------------------------------------------------
	public void onClose(CloseType closeType) { 
		GenerationEventDispatcher.removeListener(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		if (event.getType()==GenerationEventType.CHARACTER_CHANGED) {
			refresh();
		}
	}

	//-------------------------------------------------------------------
	/*
	 * Select
	 */
	private void dragDroppedAvailable(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			// Get reference for ID
			if (enhanceID.startsWith("technique:")) {
				String id = enhanceID.substring("technique:".length());
				Technique master = ShadowrunCore.getTechnique(id);
				logger.info("Try to select technique "+master);
				TechniqueValue added = control.getMartialArtsController().select(data, master);
				if (added!=null) {
					lvAvailable.getItems().remove(master);
					if (!lvSelected.getListView().getItems().contains(added))
						lvSelected.getListView().getItems().add(added);
					//        			updateCost();
				}
			}
		}
		/* let the source know whether the string was successfully
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	/*
	 * Select
	 */
	private void dragOverAvailable(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			String enhanceID = event.getDragboard().getString();
			// Get reference for ID
			if (enhanceID.startsWith("technique:")) {
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//	//-------------------------------------------------------------------
	//	/*
	//	 * Deselect
	//	 */
	//	private void dragDroppedSelected(DragEvent event) {
	//       /* if there is a string data on dragboard, read it and use it */
	//        Dragboard db = event.getDragboard();
	//        boolean success = false;
	//        if (db.hasString()) {
	//            String enhanceID = db.getString();
	//        	// Get reference for ID
	//        	if (enhanceID.startsWith("technique:")) {
	//        		String id = enhanceID.substring("lifestyleoption:".length());
	//        		LifestyleOption master = ShadowrunCore.getLifestyleOption(id);
	//         		if (master!=null) {
	//         			lvSelected.getItems().remove(master);
	//         			if (!lvAvailable.getItems().contains(master))
	//         				lvAvailable.getItems().add(master);
	//    				updateCost();
	//         		}
	//        	}
	//        }
	//        /* let the source know whether the string was successfully
	//         * transferred and used */
	//        event.setDropCompleted(success);
	//
	//        event.consume();
	//	}
	//
	//	//-------------------------------------------------------------------
	//	/*
	//	 * Deselect
	//	 */
	//	private void dragOverSelected(DragEvent event) {
	//		Node target = (Node) event.getSource();
	//		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
	//			String enhanceID = event.getDragboard().getString();
	//			// Get reference for ID
	//			if (enhanceID.startsWith("lifestyleoption:")) {
	//				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
	//			}
	//		}
	//	}

}
