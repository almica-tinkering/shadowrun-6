/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.panels;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.prelle.javafx.skin.GridPaneTableViewSkin;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.jfx.fluent.AttributeTable;
import org.prelle.shadowrun6.ShadowrunTools.PoolCalculation;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;

/**
 * @author Stefan
 *
 */
public class DerivedAttributeTable extends TableView<AttributeValue> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(AttributeTable.class.getName());

	private CharacterController ctrl;

	private TableColumn<AttributeValue, String> nameCol;
	private TableColumn<AttributeValue, Number> sumCol;

	//--------------------------------------------------------------------
	public DerivedAttributeTable(CharacterController ctrl) {
		if (ctrl==null)
			throw new NullPointerException();
		this.ctrl = ctrl;
		setSkin(new GridPaneTableViewSkin<>(this));
		initColumns();
		initValueFactories();
		initCellFactories();
		initLayout();

		for (Attribute key : Attribute.derivedTableValues()) {
			getItems().add(ctrl.getCharacter().getAttribute(key));
		}

	}

	//--------------------------------------------------------------------
	private void initColumns() {
		nameCol = new TableColumn<>(RES.getString("column.name"));
		sumCol  = new TableColumn<>(RES.getString("column.value"));

		nameCol.setId("attrtable-name");
		sumCol .setId("attrtable-sum");

		sumCol.setStyle("-fx-alignment: center");

		nameCol.setMinWidth(130);
		nameCol.setPrefWidth(160);
		sumCol.setMinWidth(40);
	}

	//--------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void initLayout() {
		getColumns().addAll(nameCol, sumCol);
	}

	//--------------------------------------------------------------------
	private void initValueFactories() {
		nameCol.setCellValueFactory(cdf -> new SimpleStringProperty(cdf.getValue().getModifyable().getName()));
		sumCol .setCellValueFactory(cdf -> new SimpleIntegerProperty(cdf.getValue().getAugmentedValue()));
	}

	//--------------------------------------------------------------------
	private void initCellFactories() {
		sumCol.setCellFactory( col -> new TableCell<AttributeValue,Number>() {
			public void updateItem(Number item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || ((Integer)item)==0) {
					setGraphic(null);
				} else {
					AttributeValue aVal = getTableRow().getItem();
					ShadowrunCharacter model = ctrl.getCharacter();
					List<PoolCalculation> modString = new ArrayList<>();
					
					Label lb = new Label(String.valueOf(item));
					lb.setText(ShadowrunTools.getInitiativeString(model, aVal.getAttribute()));
					modString = ShadowrunTools.getAttributePoolCalculation(model, aVal.getAttribute());

					if (aVal.getModifier()>aVal.getNaturalModifier())
						lb.setStyle("-fx-font-weight: bold;");
//					Tooltip tt = new Tooltip(String.join("\n", modString));
					String buf = String.join("\n",modString.stream().map(pool -> pool.value+" "+pool.source+(pool.hitAugmentLimit?"*":" ") ).collect(Collectors.toList()));
					// For initiative attributes, add modifications for dices
					String diceBuf = null;
					switch (aVal.getAttribute()) {
					case INITIATIVE_PHYSICAL:
						modString = ShadowrunTools.getAttributePoolCalculation(model, Attribute.INITIATIVE_DICE_PHYSICAL);
						diceBuf=String.join("\n",modString.stream().map(pool -> pool.value+" "+pool.source+(pool.hitAugmentLimit?"*":" ") ).collect(Collectors.toList()));
						break;
					case INITIATIVE_ASTRAL:
						modString = ShadowrunTools.getAttributePoolCalculation(model, Attribute.INITIATIVE_DICE_ASTRAL);
						diceBuf=String.join("\n",modString.stream().map(pool -> pool.value+" "+pool.source+(pool.hitAugmentLimit?"*":" ") ).collect(Collectors.toList()));
						break;
					case INITIATIVE_MATRIX:
						modString = ShadowrunTools.getAttributePoolCalculation(model, Attribute.INITIATIVE_DICE_MATRIX);
						diceBuf=String.join("\n",modString.stream().map(pool -> pool.value+" "+pool.source+(pool.hitAugmentLimit?"*":" ") ).collect(Collectors.toList()));
						break;
					case INITIATIVE_MATRIX_VR_COLD:
						modString = ShadowrunTools.getAttributePoolCalculation(model, Attribute.INITIATIVE_DICE_MATRIX_VR_COLD);
						diceBuf=String.join("\n",modString.stream().map(pool -> pool.value+" "+pool.source+(pool.hitAugmentLimit?"*":" ") ).collect(Collectors.toList()));
						break;
					case INITIATIVE_MATRIX_VR_HOT:
						modString = ShadowrunTools.getAttributePoolCalculation(model, Attribute.INITIATIVE_DICE_MATRIX_VR_HOT);
						diceBuf=String.join("\n",modString.stream().map(pool -> pool.value+" "+pool.source+(pool.hitAugmentLimit?"*":" ") ).collect(Collectors.toList()));
						break;
					}
					if (diceBuf!=null)
						buf+="\n----\n"+diceBuf;
					
					Tooltip tt = new Tooltip(buf);
					lb.setTooltip(tt);
					setGraphic(lb);
				}
			}
		});
	}


}
