/**
 * 
 */
package org.prelle.shadowrun6.gen;

import java.io.IOException;
import java.util.List;

import org.prelle.shadowrun6.CharacterConcept;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.MagicOrResonanceController;
import org.prelle.shadowrun6.charctrl.MetatypeController;

import de.rpgframework.character.HardcopyPluginData;
import de.rpgframework.genericrpg.ToDoElement;

/**
 * @author Stefan
 *
 */
public interface CharacterGenerator extends CharacterController, HardcopyPluginData {
	
	//-------------------------------------------------------------------
	public String getID();
	
	//-------------------------------------------------------------------
	public void start(ShadowrunCharacter model);
	
	//-------------------------------------------------------------------
	public void continueCreation(ShadowrunCharacter model);

	//-------------------------------------------------------------------
	public void saveCreation() throws IOException;
	
	//-------------------------------------------------------------------
	public void stop();
	
	//-------------------------------------------------------------------
	public WizardPageType[] getWizardPages();
	
	//-------------------------------------------------------------------
	public boolean hasEnoughData();
	
	//-------------------------------------------------------------------
	public boolean hasEnoughDataIgnoreMoney();

	//-------------------------------------------------------------------
	public List<ToDoElement> getToDos();

	//-------------------------------------------------------------------
	public void setConcept(CharacterConcept concept);

	//-------------------------------------------------------------------
	public CharacterConcept getConcept();

	//-------------------------------------------------------------------
	public MetatypeController getMetatypeController();

	//-------------------------------------------------------------------
	public MagicOrResonanceController getMagicOrResonanceController();
	
}
