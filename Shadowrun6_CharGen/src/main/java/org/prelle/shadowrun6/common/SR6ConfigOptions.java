package org.prelle.shadowrun6.common;

import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.ConfigOption.Type;

/**
 * @author Stefan Prelle
 *
 */
public class SR6ConfigOptions {
	
	public final static PropertyResourceBundle CONFIG_RES = (PropertyResourceBundle) ResourceBundle.getBundle(SR6ConfigOptions.class.getName());

	private final static String PROP_SKILLS_IGNORE_REQUIREMENTS      = "skills.ignore_requirements";
	private final static String PROP_METAECHOES_DONT_REFUND          = "metaechoes.dont_refund";
	private final static String PROP_METAECHOES_IGNORE_REFUND_ORDER  = "metaechoes.ignore_refund_order";
	private final static String PROP_QUALITIES_DONT_REFUND           = "qualities.dont_refund";
	private final static String PROP_QUALITIES_REFUND_FROM_CREATION  = "qualities.refund_from_creation";
	private final static String PROP_ALLOW_UNDO_FROM_CAREER   = "allow_undo_from_career";
	private final static String PROP_ALLOW_UNDO_FROM_CREATION = "allow_undo_from_creation";
	private final static String PROP_REFUND_FROM_CAREER       = "refund_from_career";
	private final static String PROP_REFUND_FROM_CREATION     = "refund_from_creation";
	private final static String PROP_PAY_GEAR                 = "pay_gear";
	private final static String PROP_HIDE_AUTOGEAR            = "hide_autogear";
	private final static String PROP_MYSTADEPT_ADVANCE_RAISE_MAGIC_RAISE_PP  = "mystic_advance_raising_magic_raises_pp";

	public static ConfigOption<Boolean> SKILLS_IGNORE_REQUIREMENTS;
	public static ConfigOption<Boolean> METAECHOES_DONT_REFUND;
	public static ConfigOption<Boolean> METAECHOES_IGNORE_REFUND_ORDER;
	public static ConfigOption<Boolean> QUALITIES_DONT_REFUND;
	public static ConfigOption<Boolean> QUALITIES_REFUND_FROM_CREATION;
	public static ConfigOption<Boolean> ALLOW_UNDO_FROM_CAREER;
	public static ConfigOption<Boolean> ALLOW_UNDO_FROM_CREATION;
	public static ConfigOption<Boolean> REFUND_FROM_CAREER;
	public static ConfigOption<Boolean> REFUND_FROM_CREATION;
	public static ConfigOption<Boolean> PAY_GEAR;
	public static ConfigOption<Boolean> HIDE_AUTOGEAR;
	public static ConfigOption<Boolean> MYSTADEPT_ADVANCE_RAISE_MAGIC_RAISE_PP;
	
	public static void attachConfigurationTree(ConfigContainer cfgShadowrun) {
		SKILLS_IGNORE_REQUIREMENTS     = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_SKILLS_IGNORE_REQUIREMENTS, Type.BOOLEAN, false);
		METAECHOES_DONT_REFUND         = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_METAECHOES_DONT_REFUND, Type.BOOLEAN, false);
		METAECHOES_IGNORE_REFUND_ORDER = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_METAECHOES_IGNORE_REFUND_ORDER, Type.BOOLEAN, false);
		QUALITIES_DONT_REFUND          = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_QUALITIES_DONT_REFUND, Type.BOOLEAN, true);
		QUALITIES_REFUND_FROM_CREATION = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_QUALITIES_REFUND_FROM_CREATION, Type.BOOLEAN, true);
		
		ALLOW_UNDO_FROM_CAREER   = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_ALLOW_UNDO_FROM_CAREER  , Type.BOOLEAN, true);
		ALLOW_UNDO_FROM_CREATION = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_ALLOW_UNDO_FROM_CREATION  , Type.BOOLEAN, false);
		REFUND_FROM_CAREER       = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_REFUND_FROM_CAREER  , Type.BOOLEAN, true);
		REFUND_FROM_CREATION     = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_REFUND_FROM_CREATION, Type.BOOLEAN, false);

		PAY_GEAR				 = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_PAY_GEAR, Type.BOOLEAN, true);
		HIDE_AUTOGEAR			 = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_HIDE_AUTOGEAR, Type.BOOLEAN, true);
		
		// Houserules
		boolean usePegasus = Locale.getDefault().getLanguage().equalsIgnoreCase(Locale.GERMAN.getLanguage());
		MYSTADEPT_ADVANCE_RAISE_MAGIC_RAISE_PP  = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_MYSTADEPT_ADVANCE_RAISE_MAGIC_RAISE_PP, Type.BOOLEAN, usePegasus);
	}

}
