/**
 * 
 */
package org.prelle.shadowrun6.chargen.proc;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CalculateGenerationEssence implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.gen");
	
	//-------------------------------------------------------------------
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {

		logger.trace("START: process");
		try {

			int sum = 0;
			for (CarriedItem item : model.getItems(false)) {
				if (Arrays.asList(ItemType.bodytechTypes()).contains(item.getUsedAsType())) {
					int essence = ShadowrunTools.getItemAttribute(model, item, ItemAttribute.ESSENCECOST).getModifiedValue();
//					float essence = (Float)item.getAsObject(ItemAttribute.ESSENCECOST);
					logger.debug("* "+item.getName()+" = "+essence);
					sum += essence;
				}
			}

			float normalLow = (6000 - sum) / 1000.0f;
			model.setEssence(normalLow);
			int magicMalus = 6 - (int)model.getEssence();
			logger.info("Remaining essence is "+normalLow+"    malus to MAGIC is "+magicMalus);
			if (magicMalus>0) {
				previous.add(new AttributeModification(Attribute.MAGIC, -magicMalus));
			}
		} finally {
			logger.trace("STOP : process() ends with "+previous.size()+" modifications still to process");
		}
		return previous;
	}

}
