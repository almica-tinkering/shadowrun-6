package org.prelle.shadowrun6.chargen;

import java.util.ResourceBundle;

/**
 * @author Stefan Prelle
 *
 */
public class SumToTenCharacterGenerator extends NewPriorityCharacterGenerator {

	//-------------------------------------------------------------------
	public SumToTenCharacterGenerator() {
		super();
		
		i18n = ResourceBundle.getBundle(SumToTenCharacterGenerator.class.getName());
		i18nHelp = ResourceBundle.getBundle(SumToTenCharacterGenerator.class.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getID()
	 */
	@Override
	public String getID() {
		return "sum_to_ten";
	}

	//--------------------------------------------------------------------
	protected PriorityTableController getPriorityTableController() {
		return new SumToTenPriorityTableController(this);
	}

}
