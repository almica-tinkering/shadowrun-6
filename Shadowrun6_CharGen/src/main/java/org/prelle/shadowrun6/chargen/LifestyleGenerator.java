/**
 * 
 */
package org.prelle.shadowrun6.chargen;

import java.util.List;

import org.prelle.shadowrun6.LifestyleValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.common.CommonLifestyleController;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author spr
 *
 */
public class LifestyleGenerator extends CommonLifestyleController {

	//-------------------------------------------------------------------
	public LifestyleGenerator(CharacterController parent) {
		super(parent);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.proc.CharacterProcessor#process(org.prelle.shadowrun.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {

		unprocessed = super.process(model, unprocessed);
		logger.trace("START: process");
		try {
			for (LifestyleValue lifestyle : model.getLifestyle()) {
				logger.info("  Pay "+lifestyle.getCost()+" for "+lifestyle.getName()+" for SIN "+lifestyle.getSIN());
				model.setNuyen( model.getNuyen() - lifestyle.getCost());
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
