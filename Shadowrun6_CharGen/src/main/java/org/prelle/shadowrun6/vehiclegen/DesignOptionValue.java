package org.prelle.shadowrun6.vehiclegen;

import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.UseAs;
import org.prelle.shadowrun6.vehicle.DesignOption;

/**
 * @author prelle
 *
 */
public class DesignOptionValue {
	
	private DesignOption ref;
	private ItemTemplate modificationItem;
	private UseAs modificationUsage;
	private int level;
	private int cost;

	//-------------------------------------------------------------------
	public DesignOptionValue(DesignOption mod) {
		this.ref = mod;
	}

	//-------------------------------------------------------------------
	public DesignOptionValue(ItemTemplate mod, UseAs usage) {
		modificationItem = mod;
		modificationUsage= usage;
		cost = (int) Math.ceil(usage.getCapacity())*2;
	}

	//-------------------------------------------------------------------
	public String toString() {
		String name = (ref!=null)?ref.getId():modificationItem.getId();
		if (level!=0)
			name+=" ("+level+")";
		return name;
	}

	//-------------------------------------------------------------------
	public DesignOption getDesignOption() {
		return ref;
	}

	//-------------------------------------------------------------------
	public int getLevel() {
		return level;
	}

	//-------------------------------------------------------------------
	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

	//-------------------------------------------------------------------
	/**
	 * @param cost the cost to set
	 */
	public void setCost(int cost) {
		this.cost = cost;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modificationItem
	 */
	public ItemTemplate getModificationItem() {
		return modificationItem;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modificationUsage
	 */
	public UseAs getModificationUsage() {
		return modificationUsage;
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (ref!=null)
			return ref.getName()+" "+level;
		if (modificationItem.hasRating())
			return modificationItem.getName()+" "+level;
		return modificationItem.getName();
	}

}
