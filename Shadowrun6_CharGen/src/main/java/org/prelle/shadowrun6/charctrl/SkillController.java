package org.prelle.shadowrun6.charctrl;

import java.util.List;

import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.SkillSpecialization;
import org.prelle.shadowrun6.SkillSpecializationValue;
import org.prelle.shadowrun6.SkillValue;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.ToDoElement;

public interface SkillController extends Controller, NumericalValueController<Skill, SkillValue> {

	//--------------------------------------------------------------------
	/**
	 * @return -1 if this does not apply, otherwise a 0+
	 */
	public int getPointsLeftSkills();

	//--------------------------------------------------------------------
	public int getPointsLeftInKnowledgeAndLanguage();

	//-------------------------------------------------------------------
	public List<Skill> getAllowedSkills();

	//-------------------------------------------------------------------
	public List<Skill> getAvailableSkills();

	//-------------------------------------------------------------------
	public List<Skill> getAvailableSkills(SkillType... types);

	//-------------------------------------------------------------------
	public boolean canBeSelected(Skill skill);

	//-------------------------------------------------------------------
	public boolean canBeSelected(SkillValue ref, SkillSpecialization spec, boolean expertise);

	//-------------------------------------------------------------------
	public SkillValue select(Skill skill);

	//-------------------------------------------------------------------
	public SkillValue selectKnowledgeOrLanguage(Skill skill, String customName);

	//-------------------------------------------------------------------
	public SkillSpecializationValue select(SkillValue ref, SkillSpecialization spec, boolean expertise);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(SkillValue ref);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(SkillValue ref, SkillSpecialization spec, boolean expertise);

	//-------------------------------------------------------------------
	public boolean deselect(SkillValue ref);

	//-------------------------------------------------------------------
	public boolean deselect(SkillValue ref, SkillSpecialization spec, boolean expertise);

	//-------------------------------------------------------------------
	public boolean canBeIncreased(SkillValue ref);

	//-------------------------------------------------------------------
	public boolean canBeDecreased(SkillValue ref);

	//-------------------------------------------------------------------
	public boolean increase(SkillValue ref);

	//-------------------------------------------------------------------
	public boolean decrease(SkillValue ref);

	//-------------------------------------------------------------------
	public boolean isRecommended(Skill val);

	//-------------------------------------------------------------------
	public boolean canSpecializeIn(SkillValue val);
	
	//-------------------------------------------------------------------
	public List<ToDoElement> getNormalToDos();
	public List<ToDoElement> getKnowledgeToDos();

	//-------------------------------------------------------------------
	public void setIgnoreRequirements(boolean value);
	public boolean isIgnoreRequirements();
	

}