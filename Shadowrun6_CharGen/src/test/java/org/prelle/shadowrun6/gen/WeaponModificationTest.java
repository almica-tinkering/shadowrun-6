package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.common.CommonEquipmentController;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemEnhancement;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemTemplate;

import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class WeaponModificationTest {

	private CommonEquipmentController control;
	private ShadowrunCharacter model;
	private List<Modification> mods;
	private int baseKarma;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		model.setNuyen(999999);
		mods = new ArrayList<>();
		ProcessorRunner charGen = new ProcessorRunner() {
			public void runProcessors() {
				model.setKarmaFree(baseKarma);
			}
		};
		model.getAttribute(Attribute.MAGIC).clearModifications();
		control = new CommonEquipmentController(charGen, model, CharGenMode.CREATING) {
			
			@Override
			public List<ConfigOption<?>> getConfigOptions() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean increaseBoughtNuyen() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public int getBoughtNuyen() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public boolean decreaseBoughtNuyen() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean canIncreaseBoughtNuyen() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean canDecreaseBoughtNuyen() {
				// TODO Auto-generated method stub
				return false;
			}
		};
	}

	//-------------------------------------------------------------------
	@Test
	public void testIdle() {
		assertEquals(0, model.getItems(false).size());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleSelection() {
		CarriedItem item = control.select(ShadowrunCore.getItem("tag_eraser"));
		assertNotNull(item); 
		
		List<ItemEnhancement> mods = control.getAvailableEnhancementsFor(item);
		assertNotNull("getAvailableEnhancementsFor() returns nullpointer", mods);
		assertTrue(mods.isEmpty());
	}

	//-------------------------------------------------------------------
	@Test
	public void testWeapon1() {
		// Firing squad
		CarriedItem item = control.select(ShadowrunCore.getItem("kami_standard"));
		assertNotNull(item); 
		
		List<ItemEnhancement> mods = control.getAvailableEnhancementsFor(item);
		assertNotNull("getAvailableEnhancementsFor() returns nullpointer", mods);
		assertFalse(mods.isEmpty());
		assertTrue(mods.contains(ShadowrunCore.getItemEnhancement("flashing_blade")));
	}

	//-------------------------------------------------------------------
	@Test
	public void testWeapon2() {
		// GRW 253
		CarriedItem item = control.select(ShadowrunCore.getItem("ares_crusader_ii"));
		assertNotNull(item); 
		assertFalse("Smartgunsystem not detected",control.getEnhancementsIn(item).isEmpty());
		
		int[] unmodifiedAR = new int[] {9,9,7,0,0};
		assertTrue(Arrays.equals(unmodifiedAR, (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getModifiedValue()));
		
		// Modify weapon now
		ItemEnhancement mod = ShadowrunCore.getItemEnhancement("barrel_extension");
		assertNotNull(mod); 
		
		assertNotNull(control.modify(item, mod));
		assertFalse(control.getEnhancementsIn(item).isEmpty());
		int[] modifiedAR = new int[] {7,9,8,0,0};
		item.getAsObject(ItemAttribute.ATTACK_RATING).getModifiedValue();
		assertTrue("Got "+Arrays.toString((int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getValue()), Arrays.equals(modifiedAR, (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getModifiedValue()));
	}

	//-------------------------------------------------------------------
	@Test
	public void testWeaponSecuritySystem() {
		// Firing squad
		CarriedItem item = control.select(ShadowrunCore.getItem("ares_crusader_ii"));
		assertNotNull(item); 
		assertFalse(control.getEnhancementsIn(item).isEmpty());
		
		assertNull(item.getSlot(ItemHook.WEAPON_SECURITY));
		List<ItemTemplate> before = control.getAvailableAccessoriesFor(item);
		
		// Modify weapon now
		ItemEnhancement mod = ShadowrunCore.getItemEnhancement("weapon_security_system");
		assertNotNull(mod); 
		
		assertNotNull(control.modify(item, mod));
		assertNotNull(item.getSlot(ItemHook.WEAPON_SECURITY));
		List<ItemTemplate> after = control.getAvailableAccessoriesFor(item);
		assertTrue(after.size()>before.size());
		assertTrue("Weapon Security modifications missing",after.contains(ShadowrunCore.getItem("wss_ap_spikes")));
	}
	
}
