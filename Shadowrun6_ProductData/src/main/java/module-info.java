/**
 * @author Stefan Prelle
 *
 */
module rpgframework.products.sr6 {
	exports de.rpgframework.products.sr6;
	opens de.rpgframework.products.sr6;

	provides de.rpgframework.products.ProductDataPlugin with de.rpgframework.products.sr6.ProductDataSR6;

	requires de.rpgframework.core;
	requires de.rpgframework.products;
}