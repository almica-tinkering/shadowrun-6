package de.rpgframework.shadowrun6.print.json;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.DummyRPGFrameworkInitCallback;
import de.rpgframework.RPGFramework;
import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.boot.StandardBootSteps;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.print.PrintType;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.prefs.Preferences;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author prelle
 *
 */
public class ExportJSONTest {

	//-------------------------------------------------------------------
	@Test
	public void loadDataTest() throws IOException {
		RPGFramework framework = RPGFrameworkLoader.getInstance();
		framework.addBootStep(StandardBootSteps.FRAMEWORK_PLUGINS);
		framework.addBootStep(StandardBootSteps.ROLEPLAYING_SYSTEMS);
		framework.initialize(new DummyRPGFrameworkInitCallback());

		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init( (percent) -> {});
		ShadowrunCore.initialize(null);
		ConfigContainer parent = new ConfigContainerImpl(Preferences.userRoot(), "foo");
		parent.createContainer("shadowrun6");
		
		
		
        ShadowrunCharacter character = ShadowrunCore.load(new FileInputStream("src/test/resources/testdata/Nahkampfadept.xml"));
        assertNotNull(character);
        System.out.println("Converting "+character.getName());
       
        JSONSR6ExportPlugin jsonPlugin = new JSONSR6ExportPlugin();
        jsonPlugin.attachConfigurationTree(parent);
        // Configure to convert to original character
        ((ConfigOption<String>)jsonPlugin.getConfiguration().get(0)).set("/tmp");
//        jsonPlugin.OPTION_EXPORT_RESOLVED.set(false);
       
        CommandResult result = jsonPlugin.handleCommand(this, CommandType.PRINT, 
        		null,
        		character,
        		null, // Scene
        		null, // ScreenManager
        		PrintType.JSON
        		);
        
       assertNotNull(result);
       assertTrue(result.wasSuccessful());
       assertNotNull(result.getReturnValue());
       System.out.println(result.getReturnValue());
	}

}
