package de.rpgframework.shadowrun6.print.json.model;

import java.util.List;

public class JSONArmor {
    public String name;
    public int rating;
    public List<JSONItemAccessory> accessories;
    public String page;
}
