package de.rpgframework.shadowrun6.print.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.rpgframework.shadowrun6.print.json.model.JSONAdeptPower;
import de.rpgframework.shadowrun6.print.json.model.JSONArmor;
import de.rpgframework.shadowrun6.print.json.model.JSONAttribute;
import de.rpgframework.shadowrun6.print.json.model.JSONAugmentation;
import de.rpgframework.shadowrun6.print.json.model.JSONCharacter;
import de.rpgframework.shadowrun6.print.json.model.JSONCloseCombatWeapon;
import de.rpgframework.shadowrun6.print.json.model.JSONComplexForm;
import de.rpgframework.shadowrun6.print.json.model.JSONContact;
import de.rpgframework.shadowrun6.print.json.model.JSONDrone;
import de.rpgframework.shadowrun6.print.json.model.JSONInitiative;
import de.rpgframework.shadowrun6.print.json.model.JSONItem;
import de.rpgframework.shadowrun6.print.json.model.JSONItemAccessory;
import de.rpgframework.shadowrun6.print.json.model.JSONLicense;
import de.rpgframework.shadowrun6.print.json.model.JSONLifestyle;
import de.rpgframework.shadowrun6.print.json.model.JSONLongRangeWeapon;
import de.rpgframework.shadowrun6.print.json.model.JSONMetaMagicOrEcho;
import de.rpgframework.shadowrun6.print.json.model.JSONQuality;
import de.rpgframework.shadowrun6.print.json.model.JSONRitual;
import de.rpgframework.shadowrun6.print.json.model.JSONSin;
import de.rpgframework.shadowrun6.print.json.model.JSONSkill;
import de.rpgframework.shadowrun6.print.json.model.JSONSkillSpecialization;
import de.rpgframework.shadowrun6.print.json.model.JSONSpell;
import de.rpgframework.shadowrun6.print.json.model.JSONVehicle;
import org.prelle.shadowrun6.AdeptPowerValue;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.ComplexFormValue;
import org.prelle.shadowrun6.Connection;
import org.prelle.shadowrun6.LicenseValue;
import org.prelle.shadowrun6.LifestyleValue;
import org.prelle.shadowrun6.MagicOrResonanceType;
import org.prelle.shadowrun6.MetamagicOrEchoValue;
import org.prelle.shadowrun6.Quality;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.Ritual;
import org.prelle.shadowrun6.RitualValue;
import org.prelle.shadowrun6.SIN;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.SkillSpecializationValue;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.OnRoadOffRoadValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class JSONSR6ExportService {

    public String exportCharacter(ShadowrunCharacter character) {
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        return gson.toJson(getJSONCharacter(character));
    }

    private JSONCharacter getJSONCharacter(ShadowrunCharacter character) {
        JSONCharacter jsonCharacter = new JSONCharacter();
        setGeneralInfo(jsonCharacter, character);
        setAttributes(jsonCharacter, character);
        setInitiatives(jsonCharacter, character);
        setSkills(jsonCharacter, character);
        setSpells(jsonCharacter, character);
        setComplexForms(jsonCharacter, character);
        setAdeptPowers(jsonCharacter, character);
        setConnections(jsonCharacter, character);
        setQualities(jsonCharacter, character);
        setLongRangeWeapons(jsonCharacter, character);
        setCloseCombatWeapons(jsonCharacter, character);
        setArmors(jsonCharacter, character);
        setItems(jsonCharacter, character);
        setAugmentations(jsonCharacter, character);
        setVehicles(jsonCharacter, character);
        setDrones(jsonCharacter, character);
        setRituals(jsonCharacter, character);
        setSINs(jsonCharacter, character);
        setLifestyles(jsonCharacter, character);
        setLicenses(jsonCharacter, character);
        return jsonCharacter;
    }

    private void setInitiatives(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONInitiative> jsonInitiativeList = new ArrayList<>();
        jsonInitiativeList.add(getJsonInitiative(character, Attribute.INITIATIVE_ASTRAL, Attribute.INITIATIVE_DICE_ASTRAL));
        jsonInitiativeList.add(getJsonInitiative(character, Attribute.INITIATIVE_PHYSICAL, Attribute.INITIATIVE_DICE_PHYSICAL));
        jsonInitiativeList.add(getJsonInitiative(character, Attribute.INITIATIVE_MATRIX, Attribute.INITIATIVE_DICE_MATRIX));
        jsonInitiativeList.add(getJsonInitiative(character, Attribute.INITIATIVE_MATRIX_VR_COLD, Attribute.INITIATIVE_DICE_MATRIX_VR_COLD));
        jsonInitiativeList.add(getJsonInitiative(character, Attribute.INITIATIVE_MATRIX_VR_HOT, Attribute.INITIATIVE_DICE_MATRIX_VR_HOT));
        jsonCharacter.initiatives = jsonInitiativeList;
    }

    private JSONInitiative getJsonInitiative(ShadowrunCharacter character, Attribute a, Attribute diceAttribute) {
        AttributeValue attribute = character.getAttribute(a);
        JSONInitiative i = new JSONInitiative();
        i.name = attribute.getAttribute().getName();
        i.id = a.name();
        i.value = attribute.getModifiedValue();
        i.dice = "+" +character.getAttribute(diceAttribute).getModifiedValue() + "D6";
        return i;
    }

    private void setLicenses(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONLicense> jsonLicenseList = new ArrayList<>();
        for (LicenseValue license : character.getLicenses()) {
            JSONLicense jsonLicense = new JSONLicense();
            jsonLicense.name = license.getName();
            UUID sin = license.getSIN();
            if (sin != null) {
                jsonLicense.sin =  character.getSIN(sin).getName();
            }
            jsonLicense.type = license.getType().getName();
            jsonLicense.rating = license.getRating().name();
            jsonLicenseList.add(jsonLicense);
        }
        jsonCharacter.licenses = jsonLicenseList;
    }

    private void setLifestyles(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONLifestyle> jsonLifestyleList = new ArrayList<>();
        for (LifestyleValue lifestyleValue : character.getLifestyle()) {
            JSONLifestyle jsonLifestyle = new JSONLifestyle();
            jsonLifestyle.customName = lifestyleValue.getName();
            jsonLifestyle.name = lifestyleValue.getLifestyle().getName();
            jsonLifestyle.cost = ShadowrunTools.getLifestyleCost(character, lifestyleValue);
            jsonLifestyle.paidMonths = lifestyleValue.getPaidMonths();
            jsonLifestyle.sin = lifestyleValue.getSIN() != null ? character.getSIN(lifestyleValue.getSIN()).getName() : null;
            jsonLifestyle.options = lifestyleValue.getOptions().stream().map(o -> o.getOption().getName()).collect(Collectors.toList());
            jsonLifestyleList.add(jsonLifestyle);
        }
        jsonCharacter.lifestyles = jsonLifestyleList;
    }

    private void setSINs(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONSin> jsonSinList = new ArrayList<>();
        for (SIN sin : character.getSINs()) {
            JSONSin jsonSin = new JSONSin();
            jsonSin.name = sin.getName();
            jsonSin.description = sin.getDescription();
            jsonSin.quality = sin.getQualityValue();
            jsonSinList.add(jsonSin);
        }
        jsonCharacter.sins = jsonSinList;
    }

    private void setRituals(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONRitual> jsonRitualList = new ArrayList<>();
        for (RitualValue ritualValue : character.getRituals()) {
            JSONRitual jsonRitual = new JSONRitual();
            Ritual ritual = ritualValue.getModifyable();
            jsonRitual.name = ritual.getName();
            jsonRitual.features = ritual.getFeatures().stream().map(r -> r.getFeature().getName()).collect(Collectors.toList());
            jsonRitual.threshold = ritual.getThreshold();
            jsonRitual.page = getPageString(ritual.getPage(), ritual.getProductNameShort());
            jsonRitualList.add(jsonRitual);
        }
        jsonCharacter.rituals = jsonRitualList;
    }

    private void setDrones(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONDrone> jsonDroneList = new ArrayList<>();
        for (CarriedItem item : character.getItems(true, ItemType.droneTypes())) {
            JSONDrone drone = new JSONDrone();
            drone.name = item.getName();
            drone.count = item.getCount();
            drone.handlOn = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.HANDLING).getModifiedValue()).getOnRoad();
            drone.handlOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.HANDLING).getModifiedValue()).getOffRoad();
            drone.speed = item.getAsValue(ItemAttribute.SPEED).getModifiedValue();
            drone.accelOn = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue()).getOnRoad();
            drone.accelOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue()).getOffRoad();
            drone.speedIntOn = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.SPEED_INTERVAL).getModifiedValue()).getOnRoad();
            drone.speedIntOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.SPEED_INTERVAL).getModifiedValue()).getOffRoad();
            drone.body = item.getAsValue(ItemAttribute.BODY).getModifiedValue();
            drone.armor = item.getAsValue(ItemAttribute.ARMOR).getModifiedValue();
            drone.pilot = item.getAsValue(ItemAttribute.PILOT).getModifiedValue();
            drone.sensor = item.getAsValue(ItemAttribute.SENSORS).getModifiedValue();
            drone.page = getPageString(item.getItem().getPage(), item.getItem().getProductNameShort());
            jsonDroneList.add(drone);
        }
        jsonCharacter.drones = jsonDroneList;
    }

    private void setVehicles(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONVehicle> jsonVehicleList = new ArrayList<>();
        for (CarriedItem item : character.getItems(false, ItemType.VEHICLES)) {
            JSONVehicle vehicle = new JSONVehicle();
            vehicle.name = item.getName();
            vehicle.handlOn = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.HANDLING).getModifiedValue()).getOnRoad();
            vehicle.handlOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.HANDLING).getModifiedValue()).getOffRoad();
            vehicle.pilot = item.getAsValue(ItemAttribute.PILOT).getModifiedValue();
            vehicle.body = item.getAsValue(ItemAttribute.BODY).getModifiedValue();
            vehicle.accelOn = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue()).getOnRoad();
            vehicle.accelOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.ACCELERATION).getModifiedValue()).getOffRoad();
            vehicle.speedIntOn = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.SPEED_INTERVAL).getModifiedValue()).getOnRoad();
            vehicle.speedIntOff = ((OnRoadOffRoadValue)item.getAsObject(ItemAttribute.SPEED_INTERVAL).getModifiedValue()).getOffRoad();
            vehicle.speed = item.getAsValue(ItemAttribute.SPEED).getModifiedValue();
            vehicle.armor = item.getAsValue(ItemAttribute.ARMOR).getModifiedValue();
            vehicle.sensor = item.getAsValue(ItemAttribute.SENSORS).getModifiedValue();
            List<JSONItemAccessory> jsonItemAccessoryList = new ArrayList<>();
            for (CarriedItem userAddedAccessory : item.getUserAddedAccessories()) {
                JSONItemAccessory accessory = new JSONItemAccessory();
                accessory.name = userAddedAccessory.getName();
                jsonItemAccessoryList.add(accessory);
            }
            vehicle.accessories = jsonItemAccessoryList;
            vehicle.page = getPageString(item.getItem().getPage(), item.getItem().getProductNameShort());
            jsonVehicleList.add(vehicle);
        }
        jsonCharacter.vehicles = jsonVehicleList;
    }

    private void setAugmentations(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONAugmentation> jsonAugmentationList = new ArrayList<>();
        for (CarriedItem item : character.getItems(true, ItemType.bodytechTypes())) {
            JSONAugmentation jsonAugmentation = new JSONAugmentation();
            jsonAugmentation.name = item.getName();
            jsonAugmentation.level = item.getRating() > 0 ? String.valueOf(item.getRating()) : "-";
            jsonAugmentation.essence = (float) ShadowrunTools.getItemAttribute(character, item, ItemAttribute.ESSENCECOST).getModifiedValue() / 1000.0f;
            jsonAugmentation.page = getPageString(item.getItem().getPage(), item.getItem().getProductNameShort());
            jsonAugmentationList.add(jsonAugmentation);
        }
        jsonCharacter.augmentations = jsonAugmentationList;
    }

    private void setAdeptPowers(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONAdeptPower> jsonAdeptPowerList = new ArrayList<>();
        for (AdeptPowerValue adeptPower : character.getAdeptPowers()) {
            JSONAdeptPower jsonAdeptPower = new JSONAdeptPower();
            jsonAdeptPower.name = adeptPower.getName();
            jsonAdeptPower.activation = adeptPower.getModifyable().getActivation().name();
            jsonAdeptPower.cost = adeptPower.getCost();
            jsonAdeptPower.page = getPageString(adeptPower.getModifyable().getPage(), adeptPower.getModifyable().getProductNameShort());
            jsonAdeptPowerList.add(jsonAdeptPower);
        }
        jsonCharacter.adeptPowers = jsonAdeptPowerList;
    }

    private void setArmors(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONArmor> jsonArmorList = new ArrayList<>();
        for (CarriedItem carriedItem : character.getItemsRecursive(true, ItemType.ARMOR)) {
            JSONArmor jsonArmor = new JSONArmor();
            jsonArmor.name = carriedItem.getName();
            jsonArmor.rating = carriedItem.getAsValue(ItemAttribute.ARMOR).getModifiedValue();
            jsonArmor.accessories = getItemAccessories(carriedItem);
            jsonArmor.page = getPageString(carriedItem.getItem().getPage(), carriedItem.getItem().getProductNameShort());
            jsonArmorList.add(jsonArmor);
        }
        jsonCharacter.armors = jsonArmorList;
    }

    private JSONItem getJsonItem(CarriedItem item) {
        JSONItem jsonItem = new JSONItem();
        jsonItem.name = item.getName();
        jsonItem.type = item.getUsedAsType().name();
        jsonItem.subType = item.getUsedAsSubType();
        jsonItem.count = item.getCount();
        jsonItem.accessories = getItemAccessories(item);
        jsonItem.page = getPageString(item.getItem().getPage(), item.getItem().getProductNameShort());
        return jsonItem;
    }

    private void setComplexForms(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONComplexForm> jsonComplexFormList = new ArrayList<>();
        for (ComplexFormValue complexForm : character.getComplexForms()) {
            JSONComplexForm jsonComplexForm = new JSONComplexForm();
            jsonComplexForm.name = complexForm.getName();
            jsonComplexForm.duration = complexForm.getModifyable().getDuration().getName();
            jsonComplexForm.fading = complexForm.getModifyable().getFading();
            jsonComplexForm.influences = complexForm.getInfluences().stream().map(BasePluginData::getName).collect(Collectors.toList());
            jsonComplexForm.page = getPageString(complexForm.getModifyable().getPage(), complexForm.getModifyable().getProductNameShort());

        }

        jsonCharacter.complexForms = jsonComplexFormList;
    }

    private void setCloseCombatWeapons(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONCloseCombatWeapon> jsonCloseCombatWeaponList = new ArrayList<>();
        for (CarriedItem closeCombatWeapon : getCloseCombatWeapons(character)) {
            JSONCloseCombatWeapon jsonCloseCombatWeapon = new JSONCloseCombatWeapon();
            jsonCloseCombatWeapon.name = closeCombatWeapon.getName();
            jsonCloseCombatWeapon.pool = ShadowrunTools.getWeaponPool(character, closeCombatWeapon);
            jsonCloseCombatWeapon.damage = ShadowrunTools.getWeaponDamage(character, closeCombatWeapon).toString();
            jsonCloseCombatWeapon.attackRating = ShadowrunTools.getAttackRatingString((int[]) closeCombatWeapon.getAsObject(ItemAttribute.ATTACK_RATING).getModifiedValue());
            jsonCloseCombatWeapon.wifi = closeCombatWeapon.getWiFiAdvantageStrings();
            jsonCloseCombatWeapon.accessories = getItemAccessories(closeCombatWeapon);
            jsonCloseCombatWeapon.page = getPageString(closeCombatWeapon.getItem().getPage(), closeCombatWeapon.getItem().getProductNameShort());
            jsonCloseCombatWeaponList.add(jsonCloseCombatWeapon);
        }
        jsonCharacter.closeCombatWeapons = jsonCloseCombatWeaponList;
    }

    private void setLongRangeWeapons(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONLongRangeWeapon> jsonLongRangeWeapons = new ArrayList<>();
        for (CarriedItem longRangeWeapon : getLongRangeWeapons(character)) {
            JSONLongRangeWeapon jsonLongRangeWeapon = new JSONLongRangeWeapon();
            jsonLongRangeWeapon.name = longRangeWeapon.getName();
            jsonLongRangeWeapon.pool = ShadowrunTools.getWeaponPool(character, longRangeWeapon);
            jsonLongRangeWeapon.damage = ShadowrunTools.getWeaponDamage(character, longRangeWeapon).toString();
            jsonLongRangeWeapon.attackRating = ShadowrunTools.getAttackRatingString((int[]) longRangeWeapon.getAsObject(ItemAttribute.ATTACK_RATING).getModifiedValue());
            jsonLongRangeWeapon.mode = (String) longRangeWeapon.getAsObject(ItemAttribute.MODE).getValue();
            jsonLongRangeWeapon.ammunition = ShadowrunTools.getItemAttributeString(character, longRangeWeapon, ItemAttribute.AMMUNITION);
            jsonLongRangeWeapon.wifi = longRangeWeapon.getWiFiAdvantageStrings();
            jsonLongRangeWeapon.accessories = getItemAccessories(longRangeWeapon);
            jsonLongRangeWeapon.page = getPageString(longRangeWeapon.getItem().getPage(), longRangeWeapon.getItem().getProductNameShort());
            jsonLongRangeWeapons.add(jsonLongRangeWeapon);
        }
        jsonCharacter.longRangeWeapons = jsonLongRangeWeapons;
    }

    private void setQualities(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONQuality> jsonQualities = new ArrayList<>();
        for (QualityValue qualityValue : character.getQualities()) {
            JSONQuality jsonQuality = new JSONQuality();
            Quality quality = qualityValue.getModifyable();
            jsonQuality.name = quality.getName();
            jsonQuality.choice = qualityValue.getNameSecondLine();
            jsonQuality.id = quality.getId();
            jsonQuality.positive = quality.getType().equals(Quality.QualityType.POSITIVE);
            jsonQuality.page = getPageString(quality.getPage(), quality.getProductNameShort());
            jsonQualities.add(jsonQuality);
        }
        jsonCharacter.qualities = jsonQualities;
    }

    private void setConnections(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONContact> jsonContacts = new ArrayList<>();
        for (Connection connection : character.getConnections()) {
            JSONContact jsonContact = new JSONContact();
            jsonContact.name = connection.getName();
            jsonContact.type = connection.getType();
            jsonContact.loyalty = connection.getLoyalty();
            jsonContact.influence = connection.getInfluence();
            jsonContact.description = connection.getDescription();
            jsonContacts.add(jsonContact);
        }
        jsonCharacter.contacts = jsonContacts;
    }

    private void setItems(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONItem> jsonItems = new ArrayList<>();
        List<CarriedItem> items = character.getItems(true, ItemType.gearTypes());
        items.addAll(character.getItems(true, ItemType.MAGICAL));
        List<CarriedItem> filteredItems = new ArrayList<>();
        for (CarriedItem item : items) {
            ItemType type = item.getUsedAsType();
            if (type != ItemType.ARMOR) {
                filteredItems.add(item);
            }
        }

        for (CarriedItem item : filteredItems) {
            JSONItem jsonItem = getJsonItem(item);
            jsonItems.add(jsonItem);
        }
        jsonCharacter.items = jsonItems;
    }

    private List<JSONItemAccessory> getItemAccessories(CarriedItem item) {
        List<JSONItemAccessory> jsonItemAccessoryList = new ArrayList<>();
        for (CarriedItem userAddedAccessory : item.getUserAddedAccessories()) {
            JSONItemAccessory jsonItemAccessory = new JSONItemAccessory();
            jsonItemAccessory.name = userAddedAccessory.getName();
            jsonItemAccessoryList.add(jsonItemAccessory);
        }
        return jsonItemAccessoryList;
    }

    private void setSpells(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONSpell> jsonSpells = new ArrayList<>();
        for (SpellValue spellValue : character.getSpells()) {
            JSONSpell jsonSpell = new JSONSpell();
            Spell spell = spellValue.getModifyable();
            jsonSpell.name = spell.getName();
            jsonSpell.id = spell.getId();
            jsonSpell.category = spell.getCategory().getName();
            jsonSpell.type = spell.getType().getName();
            jsonSpell.duration = spell.getDuration().getName();
            jsonSpell.range = spell.getRange().getName();
            jsonSpell.drain = spell.getDrain();
            jsonSpell.page = getPageString(spell.getPage(), spell.getProductNameShort());
            jsonSpell.influencedBy = spellValue.getInfluences().stream().map(BasePluginData::getName).collect(Collectors.toList());
            jsonSpells.add(jsonSpell);
        }
        jsonCharacter.spells = jsonSpells;
    }

    private String getPageString(int page, String productNameShort) {
        String pageBook;
        if (page == 0) {
            pageBook = " ";
        } else {
            pageBook = String.format("%s %s", productNameShort, page);
        }
        return pageBook;
    }

    private void setSkills(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONSkill> jsonSkills = new ArrayList<>();
        for (SkillValue skillValue : character.getSkillValues()) {
            jsonSkills.add(getJSONSkill(skillValue, character));
        }
        jsonCharacter.skills = jsonSkills;
    }

    private JSONSkill getJSONSkill(SkillValue skillValue, ShadowrunCharacter character) {
        JSONSkill jsonSkill = new JSONSkill();
        jsonSkill.name = skillValue.getName();
        jsonSkill.id = skillValue.getModifyable().getId();
        Attribute attr = skillValue.getModifyable().getAttribute1();
        jsonSkill.attribute = attr.getName();
        jsonSkill.rating = skillValue.getPoints();
        jsonSkill.pool = ShadowrunTools.getSkillPool(character, skillValue.getModifyable());
        List<SkillSpecializationValue> specializations = skillValue.getSkillSpecializations();
        List<JSONSkillSpecialization> jsonSkillSpecializationList = new ArrayList<>();
        for (SkillSpecializationValue specialization : specializations) {
            JSONSkillSpecialization jsonSpec = new JSONSkillSpecialization();
            Attribute specAttr = attr;
            if (specialization.getSpecial().getAttribute() != null) {
                specAttr = specialization.getSpecial().getAttribute();
            }
            jsonSpec.attribute = specAttr.getName();
            jsonSpec.name = specialization.getName();
            jsonSpec.id = specialization.getSpecial().getId();
            jsonSpec.expertise = specialization.isExpertise();
            int mod = specialization.isExpertise() ? 3 : 2;
            jsonSpec.pool = mod + ShadowrunTools.getSkillPool(character, skillValue.getModifyable(), specAttr);
            jsonSkillSpecializationList.add(jsonSpec);
        }
        jsonSkill.specializations = jsonSkillSpecializationList;
        jsonSkill.influencedBy = skillValue.getInfluences().stream().map(BasePluginData::getName).collect(Collectors.toList());
        return jsonSkill;
    }

    private void setAttributes(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        List<JSONAttribute> jsonAttributes = new ArrayList<>();
        Collection<AttributeValue> attributes = character.getAttributes(Attribute.attributesToSave());
        attributes.addAll(character.getAttributes(Attribute.derivedValues()));
        attributes = attributes.stream().filter(a -> !a.getAttribute().equals(Attribute.INITIATIVE_ASTRAL) && !a.getAttribute().equals(Attribute.INITIATIVE_MATRIX) && !a.getAttribute().equals(Attribute.INITIATIVE_PHYSICAL)).collect(Collectors.toList());
        for (AttributeValue attribute : attributes) {
            jsonAttributes.add(getJSONAttribute(attribute));
        }
        jsonCharacter.attributes = jsonAttributes;
    }

    private JSONAttribute getJSONAttribute(AttributeValue attribute) {
        JSONAttribute jsonAttribute = new JSONAttribute();
        jsonAttribute.name = attribute.getModifyable().getName();
        jsonAttribute.id = attribute.getAttribute().name();
        jsonAttribute.points = attribute.getPoints();
        jsonAttribute.modifiedValue = attribute.getModifiedValue();
        return jsonAttribute;
    }

    private void setGeneralInfo(JSONCharacter jsonCharacter, ShadowrunCharacter character) {
        jsonCharacter.name = character.getRealName();
        jsonCharacter.streetName = character.getName();
        jsonCharacter.metaType = character.getMetatype().getName();
        jsonCharacter.size = character.getSize();
        jsonCharacter.weight = character.getWeight();
        jsonCharacter.age = character.getAge();
        jsonCharacter.gender = character.getGender().toString();
        jsonCharacter.karma = character.getKarmaFree() + character.getKarmaInvested();
        jsonCharacter.freeKarma = character.getKarmaFree();
        jsonCharacter.notes = character.getNotes();
        jsonCharacter.nuyen = character.getNuyen();
        MagicOrResonanceType magicOrResonanceType = character.getMagicOrResonanceType();
        if (magicOrResonanceType != null && magicOrResonanceType.usesMagic()) {
            jsonCharacter.initiation = character.getInitiateSubmersionLevel();
            jsonCharacter.metamagic = getMetaMagicOrEcho(character);
        } else if (magicOrResonanceType != null && magicOrResonanceType.usesResonance()) {
            jsonCharacter.submersion = character.getInitiateSubmersionLevel();
            jsonCharacter.echoes = getMetaMagicOrEcho(character);
        }
    }

    private List<JSONMetaMagicOrEcho> getMetaMagicOrEcho(ShadowrunCharacter character) {
        List<JSONMetaMagicOrEcho> jsonMetaMagicOrEchoList = new ArrayList<>();
        for (MetamagicOrEchoValue metamagicOrEcho : character.getMetamagicOrEchoes()) {
            JSONMetaMagicOrEcho jsonMeta = new JSONMetaMagicOrEcho();
            jsonMeta.name = metamagicOrEcho.getName();
            jsonMeta.page = getPageString(metamagicOrEcho.getModifyable().getPage(), metamagicOrEcho.getModifyable().getProductNameShort());
            jsonMetaMagicOrEchoList.add(jsonMeta);
        }
        return jsonMetaMagicOrEchoList;
    }

    public static List<CarriedItem> getLongRangeWeapons(ShadowrunCharacter character) {
        List<CarriedItem> result = new ArrayList<>();
        List<CarriedItem> items = character.getItemsRecursive(true, ItemType.weaponTypes());
        for (CarriedItem weapon : items) {
            if (!weapon.isType(ItemType.WEAPON_CLOSE_COMBAT)) {
                result.add(weapon);
            }
        }
        return result;
    }

    public static List<CarriedItem> getCloseCombatWeapons(ShadowrunCharacter character) {
        List<CarriedItem> result = new ArrayList<>();
        List<CarriedItem> items = character.getItemsRecursive(true, ItemType.weaponTypes());
        for (CarriedItem weapon : items) {
            if (weapon.isType(ItemType.WEAPON_CLOSE_COMBAT)) {
                result.add(weapon);
            }
        }
        return result;
    }
}
