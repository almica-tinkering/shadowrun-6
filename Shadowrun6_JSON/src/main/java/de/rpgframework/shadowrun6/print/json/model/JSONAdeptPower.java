package de.rpgframework.shadowrun6.print.json.model;

public class JSONAdeptPower {
    public String name;
    public String activation;
    public float cost;
    public String page;
}
