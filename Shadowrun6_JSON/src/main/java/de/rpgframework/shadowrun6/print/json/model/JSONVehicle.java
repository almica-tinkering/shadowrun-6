package de.rpgframework.shadowrun6.print.json.model;

import java.util.List;

public class JSONVehicle {
    public String name;
    public int handlOn;
    public int handlOff;
    public int pilot;
    public int body;
    public int accelOn;
    public int accelOff;
    public int speedIntOn;
    public int speedIntOff;
    public int speed;
    public int armor;
    public int sensor;
    public List<JSONItemAccessory> accessories;
    public String page;
}
