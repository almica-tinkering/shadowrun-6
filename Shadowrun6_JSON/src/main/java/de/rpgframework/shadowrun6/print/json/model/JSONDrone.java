package de.rpgframework.shadowrun6.print.json.model;

public class JSONDrone {
    public String name;
    public int count;
    public int handlOn;
    public int handlOff;
    public int accelOn;
    public int accelOff;
    public int speedIntOn;
    public int speedIntOff;
    public int speed;
    public int body;
    public int armor;
    public int pilot;
    public int sensor;
    public String page;


}
