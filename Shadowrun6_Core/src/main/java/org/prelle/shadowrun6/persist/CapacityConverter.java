/**
 *
 */
package org.prelle.shadowrun6.persist;

import org.prelle.shadowrun6.items.CapacityDefinitions;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author Stefan
 *
 */
public class CapacityConverter implements StringValueConverter<Float> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public Float read(String v) throws Exception {
		v = v.trim();
		
		if (v.equals("BODY/3")) return CapacityDefinitions.BODY_DIV_3;
		if (v.equals("BODY/2")) return CapacityDefinitions.BODY_DIV_2;
		if (v.equals("ARMOR/4")) return CapacityDefinitions.ARMOR_DIV_4;
		if (v.equals("RATING-1")) return CapacityDefinitions.RATING_MINUS_1;
		if (v.contains("/")) {
			System.err.println("CapacityConverter: Unknown key: '"+v+"'");
			throw new IllegalArgumentException("Unknown key: '"+v+"'");
		}
		return Float.valueOf(v);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(Float v) throws Exception {
		if (v==CapacityDefinitions.BODY_DIV_3) return "BODY/3";
		if (v==CapacityDefinitions.BODY_DIV_2) return "BODY/2";
		if (v==CapacityDefinitions.ARMOR_DIV_4) return "ARMOR/4";
		if (v==CapacityDefinitions.RATING_MINUS_1) return "RATING-1";
		return String.valueOf(v);
	}

}
