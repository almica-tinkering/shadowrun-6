/**
 * 
 */
package org.prelle.shadowrun6.persist;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.StartElement;

import org.prelle.simplepersist.XMLElementConverter;
import org.prelle.simplepersist.marshaller.XmlNode;
import org.prelle.simplepersist.unmarshal.XMLTreeItem;

/**
 * @author Stefan
 *
 */
public class MappedModificationConverter implements XMLElementConverter<Integer> {

	//--------------------------------------------------------------------
	/**
	 */
	public MappedModificationConverter() {
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("exports")
	@Override
	public void write(XmlNode node, Integer value) throws Exception {
		node.setAttribute("value",  String.valueOf(value));
	}

	@SuppressWarnings("exports")
	@Override
	public Integer read(XMLTreeItem node, StartElement ev, XMLEventReader evRd) throws Exception {
		try {
			String f1 = ev.getAttributeByName(new QName("value")).getValue();
			return Integer.parseInt(f1);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

}
