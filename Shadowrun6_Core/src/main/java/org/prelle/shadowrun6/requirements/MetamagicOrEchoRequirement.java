package org.prelle.shadowrun6.requirements;

import org.prelle.shadowrun6.MetamagicOrEcho;
import org.prelle.shadowrun6.persist.MetamagicOrEchoConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name = "metaechoreq")
public class MetamagicOrEchoRequirement extends Requirement {

	@org.prelle.simplepersist.Attribute
	@AttribConvert(MetamagicOrEchoConverter.class)
    private MetamagicOrEcho ref;
    
    //-----------------------------------------------------------------------
    public MetamagicOrEchoRequirement() {
    }
    
    //-----------------------------------------------------------------------
    public MetamagicOrEchoRequirement(MetamagicOrEcho attr) {
        this.ref = attr;
    }
    
    //-----------------------------------------------------------------------
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
   
    //-----------------------------------------------------------------------
    public String toString() {
        return String.valueOf(ref);
    }
 
    //-----------------------------------------------------------------------
    public MetamagicOrEcho getMetamagicOrEcho() {
        return ref;
    }
   
    //-----------------------------------------------------------------------
    public Object clone() {
        return new MetamagicOrEchoRequirement(ref);
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof MetamagicOrEchoRequirement) {
            MetamagicOrEchoRequirement amod = (MetamagicOrEchoRequirement)o;
            return amod.getMetamagicOrEcho()!=ref;
        } else
            return false;
    }
   
}
