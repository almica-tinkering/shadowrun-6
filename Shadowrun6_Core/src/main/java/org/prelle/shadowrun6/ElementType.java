package org.prelle.shadowrun6;

/**
 * @author Stefan Prelle
 *
 */
public enum ElementType {

	FIRE,
	COLD,
	ELECTRICITY,
	CHEMICAL
	;

	//-------------------------------------------------------------------
	public String getName() {
		return Resource.get(ShadowrunCore.getI18nResources(),"element."+this.name().toLowerCase());
	}

}
