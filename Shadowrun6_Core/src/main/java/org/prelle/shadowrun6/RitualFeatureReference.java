/**
 * 
 */
package org.prelle.shadowrun6;

import org.prelle.shadowrun6.persist.RitualFeatureConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */

public class RitualFeatureReference {
	
	@Attribute
	@AttribConvert(RitualFeatureConverter.class)
	private RitualFeature ref;

	//-------------------------------------------------------------------
	public RitualFeatureReference() {	
	}

	//-------------------------------------------------------------------
	public RitualFeatureReference(RitualFeature feat) {
		this.ref = feat;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return String.valueOf(ref);
	}

	//-------------------------------------------------------------------
	public RitualFeature getFeature() {
		return ref;
	}

}
