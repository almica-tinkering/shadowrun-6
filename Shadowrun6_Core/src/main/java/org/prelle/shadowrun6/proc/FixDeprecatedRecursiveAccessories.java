/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.items.CarriedItem;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class FixDeprecatedRecursiveAccessories implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.proc.items");
	
	//-------------------------------------------------------------------
	public FixDeprecatedRecursiveAccessories() {
	}

	//-------------------------------------------------------------------
	private void check(ShadowrunCharacter model, CarriedItem container) {
		for (CarriedItem accessory : container.getUserAddedAccessories()) {
			if (container.removeAccessory(accessory)) { // Remove from old structure
				logger.warn("FIX DEPRECATED ACCESSORY STORAGE: "+accessory.getItem().getId());
				model.addItem(accessory);
				//			container.addAccessory(accessory); // Add to new structure
				accessory.setEmbeddedIn(container.getUniqueId());
				container.addAccessory(accessory);
				// Recurse
				check(model, accessory);
			} else {
				logger.error("Could not remove accessory from deprecated place");
				System.err.println("Could not remove accessory from deprecated place");

			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			for (CarriedItem ref :model.getItemsAddedByUser()) {
				check(model, ref);
			}
			for (CarriedItem ref :model.getAutoItemAccessories()) {
				CarriedItem container = model.getItem(ref.getEmbeddedIn());
				if (container!=null) {
					model.addItem(ref);					
				} else {
					logger.warn("Found accessory for non-existing container: "+ref.getItem().getId());
				}
				check(model, ref);
				model.removeAutoItemAccessories(ref);
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
