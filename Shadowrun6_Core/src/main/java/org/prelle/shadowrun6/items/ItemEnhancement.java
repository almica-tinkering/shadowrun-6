/**
 *
 */
package org.prelle.shadowrun6.items;

import java.text.Collator;
import java.util.List;
import java.util.MissingResourceException;

import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.modifications.HasOptionalCondition;
import org.prelle.shadowrun6.modifications.ItemHookModification;
import org.prelle.shadowrun6.modifications.ModificationList;
import org.prelle.shadowrun6.persist.AvailabilityConverter;
import org.prelle.shadowrun6.requirements.Requirement;
import org.prelle.shadowrun6.requirements.RequirementList;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class ItemEnhancement extends BasePluginData implements Comparable<ItemEnhancement>  {

	@Attribute(required=true)
	private String id;
	@Attribute
	protected ItemType type;
	@Attribute
	protected ItemSubType subtype;
	@Attribute(name="avail",required=false)
	@AttribConvert(AvailabilityConverter.class)
	private Availability availability;
	/* Price in Nuyen */
	@Attribute(required=true)
	private int cost;
	@Attribute
	private int size = 1;
	@Attribute(name="modonly")
	private boolean selectableByModificationOnly;

	@Element
	private ModificationList modifications;
	@Element
	private RequirementList requires;

	//--------------------------------------------------------------------
	public ItemEnhancement() {
		modifications = new ModificationList();
		requires = new RequirementList();
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		return "ITEMENH:"+id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return id;
		String key = "itemmod."+id;
		try {
			return i18n.getString(key);
		} catch (MissingResourceException mre) {
			if (!reportedKeys.contains(mre.getKey())) {
				reportedKeys.add(mre.getKey());
				logger.error("Missing property '"+mre.getKey()+"' in "+i18n.getBaseBundleName());
				if (MISSING!=null)
					MISSING.println(mre.getKey()+"=");
			}
			return key;
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "itemmod."+id+".page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "itemmod."+id+".desc";
	}

	//-------------------------------------------------------------------
	public List<Modification> getModifications() {
		return modifications;
	}

	//-------------------------------------------------------------------
	public void addModification(ItemHookModification mod) {
		modifications.add(mod);
	}

	//-------------------------------------------------------------------
	public List<Requirement> getRequirements() {
		return requires;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ItemEnhancement other) {
		return Collator.getInstance().compare(getName(), other.getName());
	}

	//-------------------------------------------------------------------
	public ItemType getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the subtype
	 */
	public ItemSubType getSubtype() {
		return subtype;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the availability
	 */
	public Availability getAvailability() {
		return availability;
	}

	//-------------------------------------------------------------------
	/**
	 * @param availability the availability to set
	 */
	public void setAvailability(Availability availability) {
		this.availability = availability;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the price
	 */
	public int getPrice() {
		return cost;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the price
	 */
	public String getPriceString(ShadowrunCharacter model) {
		return cost+" \u00A5";
//		return ShadowrunTools.getCostWithMetatypeModifier(model, this, cost)+" \u00A5";
	}

	//--------------------------------------------------------------------
	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.cost = price;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	//-------------------------------------------------------------------
	public String getConditionKey(HasOptionalCondition mod) {
		return getId()+".cond."+mod.getConditionIndex();
	}

	//-------------------------------------------------------------------
	public String getConditionName(HasOptionalCondition mod) {
		String key = getId()+".cond."+mod.getConditionIndex();
		if (i18n==null) 
			return id;
		try {
			return Resource.get(i18n,key);
		} catch (MissingResourceException e) {
			if (!reportedKeys.contains(e.getKey())) {
				reportedKeys.add(e.getKey());
				logger.warn(String.format("key missing:    %s   %s", i18n.getBaseBundleName(), e.getKey()));
				if (MISSING!=null) {
					MISSING.println(e.getKey()+"=");
				}
			}
			return e.getKey();
		}
	}

	//-------------------------------------------------------------------
	public boolean isSelectableByModificationOnly() {
		return selectableByModificationOnly;
	}

	//-------------------------------------------------------------------
	public void setSelectableByModificationOnly(boolean selectableByModificationOnly) {
		this.selectableByModificationOnly = selectableByModificationOnly;
	}

}
