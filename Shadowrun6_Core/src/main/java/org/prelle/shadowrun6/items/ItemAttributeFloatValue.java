package org.prelle.shadowrun6.items;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun6.modifications.ItemAttributeModification;

import de.rpgframework.genericrpg.modification.Modification;

public class ItemAttributeFloatValue extends ItemAttributeValue  {

	protected float value;

	//--------------------------------------------------------------------
	public ItemAttributeFloatValue(ItemAttribute attr, float val, List<Modification> mods) {
		super(attr, mods);
		value = val;
	}

	//--------------------------------------------------------------------
	public ItemAttributeFloatValue(ItemAttribute attr, float val) {
		super(attr, new ArrayList<>());
		value = val;
	}

	//--------------------------------------------------------------------
	public String toString() {
		if (getModifier()==0)
			return String.valueOf(value);
		return value+" ("+getModifiedValue()+")";
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getPoints()
	 */
	public float getPoints() {
		return value;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#setPoints(int)
	 */
	public void setPoints(float points) {
		value = points;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifier()
	 */
	public float getModifier() {
		int count = 0;
		for (Modification mod : modifications) {
			if (mod instanceof ItemAttributeModification) {
				ItemAttributeModification sMod = (ItemAttributeModification)mod;
				if (sMod.getModifiedItem()==attribute)
					count += sMod.getValue();
			}
		}
		return count;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifiedValue()
	 */
	public float getModifiedValue() {
		return value + getModifier();
	}

}
