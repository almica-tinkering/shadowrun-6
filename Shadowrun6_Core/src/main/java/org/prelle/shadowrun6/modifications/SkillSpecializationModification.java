package org.prelle.shadowrun6.modifications;

import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.SkillSpecialization;
import org.prelle.shadowrun6.persist.SkillConverter;
import org.prelle.shadowrun6.persist.SkillSpecializationConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="skillspecmod")
public class SkillSpecializationModification extends ModificationBase<SkillSpecialization> {

	@Attribute
	@AttribConvert(SkillConverter.class)
	private Skill skill;
	@Attribute
	@AttribConvert(SkillSpecializationConverter.class)
	private SkillSpecialization ref;
	@Attribute(name="expert")
	private boolean expertise;

    //-----------------------------------------------------------------------
    public SkillSpecializationModification() {
    }

    //-----------------------------------------------------------------------
    public SkillSpecializationModification(Skill skill, SkillSpecialization ref) {
    	this.skill = skill;
        this.ref = ref;
    }

    //-----------------------------------------------------------------------
    public String toString() {
         return "spec="+ref+"/"+expertise+"/"+getExpCost()+" Karma";
    }

    //-----------------------------------------------------------------------
    public Skill getSkill() {
        return skill;
    }

    //-----------------------------------------------------------------------
    public void setSkill(Skill val) {
        this.skill = val;
    }

    //-----------------------------------------------------------------------
    public SkillSpecialization getSkillSpecialization() {
        return ref;
    }

    //-----------------------------------------------------------------------
    public void setSkillSpecialization(SkillSpecialization val) {
        this.ref = val;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof SkillSpecializationModification) {
            SkillSpecializationModification amod = (SkillSpecializationModification)o;
            if (amod.getSkillSpecialization()     !=ref) return false;
            if (amod.getSkill()    !=skill) return false;
            if (amod.isExpertise() !=expertise) return false;
            return true;
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
            return toString().compareTo(obj.toString());

}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getValue()
	 */
	@Override
	public int getValue() {
		return expertise?2:1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#setValue(int)
	 */
	@Override
	public void setValue(int val) {
		expertise = val>1;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the expertise
	 */
	public boolean isExpertise() {
		return expertise;
	}

	//-------------------------------------------------------------------
	/**
	 * @param expertise the expertise to set
	 */
	public void setExpertise(boolean expertise) {
		this.expertise = expertise;
	}

}