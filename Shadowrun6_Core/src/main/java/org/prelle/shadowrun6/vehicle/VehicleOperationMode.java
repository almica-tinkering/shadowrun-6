/**
 * 
 */
package org.prelle.shadowrun6.vehicle;

import org.prelle.shadowrun6.ShadowrunCore;

import de.rpgframework.ResourceI18N;

/**
 * @author Stefan
 *
 */
public enum VehicleOperationMode {
	
	DRIVING,
	RCC,
	JUMPED_IN,
	AUTONOMOUS
	;
	
	public String getName() {
		return ResourceI18N.get( ShadowrunCore.getI18nResources(), "vehiclemode."+name().toLowerCase());
	}
}
